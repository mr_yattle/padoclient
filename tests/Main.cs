using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace tests
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			MultiTiff();
		}
		
		public static void MultiTiff (string[] args)
		{
			Console.WriteLine ("Create multitiff image!");
			string saveDir = Environment.CurrentDirectory+"/Data";
			List<Image> images = new List<Image>();
			images.Add(Image.FromFile(@"Data\1.tiff"));
			images.Add(Image.FromFile(@"Data\2.tiff"));

			Image multipage = null;
			string savepath = System.IO.Path.GetFullPath(saveDir+"/document.tiff");
			Encoder enc=Encoder.SaveFlag;
			ImageCodecInfo info=null;
			foreach(ImageCodecInfo ice in ImageCodecInfo.GetImageEncoders())
				if(ice.MimeType=="image/tiff")
					info=ice;
			EncoderParameters ep=new EncoderParameters(1);
			ep.Param[0]=new EncoderParameter(enc,(long)EncoderValue.MultiFrame);
			
			foreach(Image image in images){
				if(multipage == null) {
					multipage = image;
					multipage.Save(savepath,info,ep);
					// Add others
					ep.Param[0]=new EncoderParameter(enc,(long)EncoderValue.FrameDimensionPage);
				}
				else {
					multipage.SaveAdd(image, ep);
				}
			}
			ep.Param[0]=new EncoderParameter(enc,(long)EncoderValue.Flush);
			multipage.SaveAdd(ep);
		}
		
	}
	
}

