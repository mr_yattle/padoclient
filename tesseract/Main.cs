using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Threading;

namespace TesseractConsole
{
    class Program
    {
		
        static void Main(string[] args)
        {
			string tiffile = "test.tiff";
			string saveDir = "Data";
			
			Ocr ocr = new Ocr(tiffile);
			string uid = System.Guid.NewGuid().ToString();
			string savepath = System.IO.Path.GetFullPath(saveDir+"/ocr-"+uid+".xml");
			// create a writer and open the file
            TextWriter tw = new StreamWriter(savepath);

			// write ocr xml result
			tw.Write(ocr.ToXml(tiffile, "ita"));

            // close the stream
            tw.Close();	
        }
		
		
        static void _Main(string[] args)
        {
            // Code usage sample
            _Ocr ocr = new _Ocr();
            using (Bitmap bmp = new Bitmap(@"test.tiff"))
            {
                tessnet2.Tesseract tessocr = new tessnet2.Tesseract();
                tessocr.Init(null, "ita", false);
                tessocr.GetThresholdedImage(bmp, Rectangle.Empty).Save(Guid.NewGuid().ToString() + ".tiff");
                // Tessdata directory must be in the directory than this exe
                Console.WriteLine("Multithread version");
                ocr.DoOCRMultiThred(bmp, "ita");
                Console.WriteLine("Normal version");
                ocr.DoOCRNormal(bmp, "ita");
            }
        }
    }

    public class _Ocr
    {
        public void DumpResult(List<tessnet2.Word> result)
        {
            foreach (tessnet2.Word word in result) {
                Console.WriteLine("{0} : {1} - {2} {3} {4}", 
					word.Confidence, 
				    word.Text, 
				    word.Tag, 
				    word.CharList, 
				    word.Text);
			}
        }

        public List<tessnet2.Word> DoOCRNormal(Bitmap image, string lang)
        {
            tessnet2.Tesseract ocr = new tessnet2.Tesseract();
            ocr.Init(null, lang, false);
            List<tessnet2.Word> result = ocr.DoOCR(image, Rectangle.Empty);
            DumpResult(result);
            return result;
        }

        ManualResetEvent m_event;

        public void DoOCRMultiThred(Bitmap image, string lang)
        {
            tessnet2.Tesseract ocr = new tessnet2.Tesseract();
            ocr.Init(null, lang, false);
            // If the OcrDone delegate is not null then this'll be the multithreaded version
            ocr.OcrDone = new tessnet2.Tesseract.OcrDoneHandler(Finished);
            // For event to work, must use the multithreaded version
            ocr.ProgressEvent += new tessnet2.Tesseract.ProgressHandler(ocr_ProgressEvent);
            m_event = new ManualResetEvent(false);
            ocr.DoOCR(image, Rectangle.Empty);
            // Wait here it's finished
            m_event.WaitOne();
        }

        public void Finished(List<tessnet2.Word> result)
        {
            DumpResult(result);
            m_event.Set();
        }

        void  ocr_ProgressEvent(int percent)
        {
 	        Console.WriteLine("{0}% progression", percent);
        }
		
    }
	
	/* My Ocr */
	public class Ocr
	{
		private string filepath = null;
		private string lang = null;
		//private string langs = System.IO.Path.GetFullPath(Utility.ApplicationPath()+"/Tesseract");
		private string langs = "tessdata";	
		public Ocr (string _filepath, string _lang)
		{
			filepath = _filepath;
			lang = _lang;
		}
		
		/* default Ocr */
		public Ocr(string _filepath) {
			new Ocr(_filepath, "ita");
		}
		
		/* Get words indexing in xml format */
		public string ToXml(string filepath, string lang) {
			
            using (Bitmap bmp = new Bitmap(filepath))
            {
                tessnet2.Tesseract tessocr = new tessnet2.Tesseract();
                tessocr.Init(langs, lang, false);
                tessocr.GetThresholdedImage(bmp, Rectangle.Empty).Save(Guid.NewGuid().ToString() + ".tiff");
				List<tessnet2.Word> result = tessocr.DoOCR(bmp, Rectangle.Empty);
				
				string xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
				xml += "<ocr>\n";
				foreach (tessnet2.Word word in result) {
					xml += String.Format(" <word confidence=\"{0}\">{1}</word>\n",
						word.Confidence, word.Text);
				}
				xml += "</ocr>";
				return xml;
            }
			
		}
		
	}



}