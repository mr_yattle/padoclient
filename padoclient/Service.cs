
using System;
using System.Threading;

namespace padoclient
{

	public partial class Service
	{

		//private int _wait;
		//private bool _lock;
		//private Thread _runner;
		private Settings _settings;
		private Webserver _webserver;

		public Service (Settings settings)
		{
			//this.wait = settings.updateFrequency;
			//this.settings = settings;
			//this.stopped = true;
			//this.runner = new Thread (new ThreadStart (this.run));
			this.settings = settings;
			this.webserver = new Webserver(this.settings);
		}
		
		public void start()
		{
			this.webserver.execute();
		}
		
		public void stop()
		{
			this.webserver.stop();
		}
		
	    public Settings settings 
	    {
	      get { return _settings; }
	      set { _settings = value; }
		}
		
	    public Webserver webserver 
	    {
	      get { return _webserver; }
	      set { _webserver = value; }
		}
		
		/*
	    public int wait 
	    {
	      get { return _wait; }
	      set { _wait = value; }
	    }
	    
	    public bool stopped 
	    {
	      get { return _lock; }
	      set { _lock = value; }
	    }
		
	    public Thread runner 
	    {
	      get { return _runner; }
	      set { _runner = value; }
	    }	
				
		public void start()
		{
			this.stopped = false;
			this.runner.Start();
		}
		
		public void stop()
		{
			this.stopped = true;
		}
		
		public int getUpdateFrequency()
		{

			// Cast second in millieconds
			return this.wait * 1000;
		}
		
		public void test()
		{
			while(!this.stopped) {
				Console.WriteLine ("Hello main-thread");
			}
			if(this.stopped) {
				this.runner.Abort();
			}
		}		
		
		// use thread to know if service has crash 
		// if so restart app
		public void run()
		{
			while(!this.stopped) {
				this.execute();
			}
			if(this.stopped) {
				//this.stop();
				this.runner.Abort();
			}
		}	
		*/
		
	}
}
