using System;
using System.Net;
using System.Collections.Generic;
using padoclient;

namespace GenericOS.Component
{
	public class Configure
	{
		public Configure ()
		{
		}
		
		/* get json string reading settings */
		public string retrieve(IDictionary<string, string> queryString) {
			Settings settings = new Settings();
			string ipaddresses = "";
			List<string> _ipaddresses = new List<string>();
			_ipaddresses.Add("{"+String.Format("\"ip\":\"{0}\", \"value\":\"Loopback\", \"selected\":\"{1}\"", 
				IPAddress.Loopback, IPAddress.Loopback.Equals(settings.ipaddress) ? 1 : 0)+"}");
			_ipaddresses.Add("{"+String.Format("\"ip\":\"{0}\", \"value\":\"Any\", \"selected\":\"{1}\"", 
				IPAddress.Any, IPAddress.Any.Equals(settings.ipaddress) ? 1 : 0)+"}");
			
			// Get my pc ip
			IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
			foreach (IPAddress localIP in localIPs)
			{
				_ipaddresses.Add("{"+String.Format("\"ip\":\"{0}\", \"value\":\"{1}\", \"selected\":\"{2}\"", 
					localIP, localIP, localIP.Equals(settings.ipaddress) ? 1 : 0)+"}");
			
			}

			ipaddresses = string.Join(",", _ipaddresses.ToArray());
			string json_response = String.Format("\"ipaddresses\":[{0}]," +
				"\"port\":\"{1}\", " +
				"\"username\":\"{2}\", " +
				"\"password\":\"{3}\"", 
				ipaddresses, settings.port, 
			    settings.username, settings.password);
			return "{"+json_response+"}";
		}
		
		/* get json string reading settings */
		public string save(IDictionary<string, string> queryString) {
			int status = 0;
			string errors = "";
			string messages = "";
			try {
				int port = int.Parse(queryString["port"]);
				string username = queryString["username"];
				string password = queryString["password"];
				IPAddress ipaddress = IPAddress.Parse(queryString["ipaddress"]);
				Settings settings = new Settings();
				settings.port = port;
				settings.username = username;
				settings.password = password;
				// Test ip address
				settings.ipaddress = ipaddress;
				settings.Save();
				messages = "\"Configurations saved\"";
				status = 1;
			}
			catch(Exception ex) {
				errors = String.Format("\"{0}\"", ex.Message.ToString());
				status = 2;
			}
			string json_response = String.Format("\"status\":\"{0}\", " +
				"\"errors\":[{1}], " +
				"\"messages\":[{2}]", status.ToString()
			    	, errors.ToString()
			        , messages.ToString());
			return "{"+json_response+"}";
		}
		
	}
}

