# options: -translation lf -encoding utf-8

proc ::InstallJammer::InitFiles {} {
    File ::F69DF366-C775-4802-896A-D7BC73F30CFD -name Win32.Scanner.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 57344 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::0FE921FD-9D57-4F5F-BCE3-3BC1933DB422 -name ICSharpCode.SharpZipLib.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 225280 -mtime 1290514661 -attributes 1000 -filemethod 0
    File ::6447686F-EE69-444A-8448-01E134F1BCB6 -name Kayak.Core.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 4608 -mtime 1290514661 -attributes 1000 -filemethod 0
    File ::BBA2350F-CC03-412B-81F9-FCA5D4299A29 -name Kayak.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 45056 -mtime 1290514661 -attributes 1000 -filemethod 0
    File ::9FD52826-0125-4A17-AB08-D1E2A17AF4AD -name Kayak.Framework.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 102400 -mtime 1290527990 -attributes 1000 -filemethod 0
    File ::49D8947E-2EB4-4252-8DD9-96CF7DD50ECF -name log4net.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 270336 -mtime 1289915009 -attributes 1000 -filemethod 0
    File ::934E3501-AC08-4E64-8000-A6BD9F3CB326 -name padoclient.exe -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 53248 -mtime 1290529388 -attributes 1000 -filemethod 0
    File ::83995AA8-1BC0-493E-A863-0FC7A6C7E210 -name PdfSharp.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 602112 -mtime 1290527990 -attributes 1000 -filemethod 0
    File ::AD19BBA5-320F-42B7-9165-17E3317605CB -name System.CoreEx.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 49152 -mtime 1289914996 -attributes 1000 -filemethod 0
    File ::18BC1F68-550E-4792-878A-313EC40A3647 -name System.Observable.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 4608 -mtime 1289914996 -attributes 1000 -filemethod 0
    File ::6008CF6D-25CD-47E9-9129-E1FD7A4941DC -name System.Reactive.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 331776 -mtime 1289914996 -attributes 1000 -filemethod 0
    File ::DC625BDC-6970-4CB2-9FDD-E88591AA6245 -name System.Threading.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 393216 -mtime 1289914996 -attributes 1000 -filemethod 0
    File ::91ACB0EC-F24C-4A06-9DDB-2F47AF2C3CB5 -name tessnet2_32.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 1455104 -mtime 1290425743 -attributes 1000 -filemethod 0
    File ::2A85291D-1DB0-4E90-8CFA-D2ECFE5CCFA9 -name TwainDotNet.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 57344 -mtime 1290514662 -attributes 1000 -filemethod 0
    File ::2252AA41-902D-4C26-8D5E-BAE5BE4D3DCD -name TwainDotNet.WinFroms.dll -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 5632 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::02592299-8E38-4CA9-B5BE-D4717B8853C8 -name Capabilites -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Capabilites -type dir -attributes 0000 -filemethod 0
    File ::3BE56774-5881-4958-BC40-5ABF00E33312 -name {FUJITSU fi-5750Cdj.xml} -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Capabilites -size 7826 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::5EBD2B20-33BB-4420-A2D0-2DAA2FCA1208 -name {Kofax Software VRS - TWAIN.xml} -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Capabilites -size 7839 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::1D0D3CD6-23B3-4DEB-AE03-E2DEB859A59F -name {Type3045 Scanner.xml} -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Capabilites -size 7833 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::283336FC-1B53-4BE4-B55E-E74435E77FAD -name WIA-fi-5750Cdj.xml -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Capabilites -size 7866 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::6997C1C8-430D-454B-9072-791DD919C211 -name {WIA-HP Scanjet G3010.xml} -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Capabilites -size 7883 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::F0C31BA3-F624-4719-82CF-C6C4500F0710 -name Css -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Css -type dir -attributes 0000 -filemethod 0
    File ::53725868-6E8D-4E72-B05B-729B8FA2BDFE -name main.css -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Css -size 6372 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::E5C9A503-34C2-43D6-B775-C7FA66ED40B9 -name Data -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Data -type dir -attributes 0000 -filemethod 0
    File ::295A57DF-1323-478C-B921-9CDE7567B2E1 -name README.txt -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Data -size 39 -mtime 1290527991 -attributes 1000 -filemethod 0
    File ::594A3742-E225-43E9-B227-B6CA7312FBFF -name Scanner -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Data/Scanner -type dir -attributes 0000 -filemethod 0
    File ::25EDF128-987C-4CB4-879B-6635CAC6D0AB -name Images -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -type dir -attributes 0000 -filemethod 0
    File ::CB7F6C7B-0DBE-4AB2-A4F7-C3821F7186D2 -name ajax-loader.gif -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 1849 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::A2FBEE3F-16D9-4BF5-A196-D3708119C2A5 -name background-title.gif -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 71 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::0F05AD02-C43D-4F65-89C2-2FE2EA67CDBD -name configuration-title-icon.gif -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 1467 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::9BFC61C3-9C4E-4373-A28D-C4F38D727987 -name default-title-icon.gif -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 1429 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::F98AC004-A8E8-44EB-9367-0481FD6ACE75 -name dropshadow.gif -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 4398 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::535FE0CB-3055-42E4-92C2-65C189886136 -name favicon.ico -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 1150 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::E67963E9-DC03-442B-94C5-D2C4EAAFEE09 -name null.gif -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 807 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::B147A46D-2A0B-418B-8BBE-58C13E7911E1 -name padoclient.gif -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 1429 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::515B9568-13B3-4E4C-B1B4-BA7D37FACC50 -name scanner-download-pdf.png -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 1488 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::EF55CA1C-EE8E-44D5-88A6-8C8B3839CFEF -name scanner-download-tiff.png -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 1457 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::82A970B1-7BBA-45DA-9099-635F0C7E8449 -name scanner-palette-bw.png -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 196 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::86A76661-FF6C-4824-B759-1854F13D53D9 -name scanner-palette-color.png -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 317 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::C63D5386-B2C5-4809-89BF-922876814AD3 -name scanner-palette-grayscale.png -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 240 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::2C429AB4-8AB5-4549-B61F-A6DF3FEE2E06 -name scanner-title-icon.gif -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 1427 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::89260CA0-770E-4CC1-B67A-2C4AAA93C130 -name scanner.ico -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Images -size 4710 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::D4431CA8-B4C2-43E9-A143-77666B4806B6 -name Javascript -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Javascript -type dir -attributes 0000 -filemethod 0
    File ::34898B9C-4979-46D5-884B-F8A86EC16986 -name commons.js -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Javascript -size 1736 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::168E9A95-09B5-47FD-A409-4D3BCE0EE6FE -name configure.js -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Javascript -size 4739 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::CF3C691E-38A0-4214-8DE8-65F7BF2A559C -name jquery.form.js -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Javascript -size 23248 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::4CAD8287-CE09-4033-9619-01F8CCDCB442 -name jquery.js -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Javascript -size 187342 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::06A86D60-4E5F-4B36-B1EC-B198F2050A16 -name md5.js -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Javascript -size 12297 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::389A5F17-23B0-439C-B3C6-AEBAC7CD352D -name scanner.js -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Javascript -size 5960 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::29F35631-940D-4C03-88ED-28CED92AFF22 -name Logs -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Logs -type dir -attributes 0000 -filemethod 0
    File ::55A483C7-CAC3-4B88-B8A1-6C3D5C6FA9CD -name padoclient.log -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Logs -size 16213 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::02039ACB-8689-4BA9-9FD8-0752DE9B32BB -name README.txt -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Logs -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::A1180916-B60D-4CD3-8D02-1E602AAE23B9 -name win32.scanner.log -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Logs -size 45337 -mtime 1290528616 -attributes 1000 -filemethod 0
    File ::9E52DD23-9B34-404C-9599-03313CDA7F92 -name Templates -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates -type dir -attributes 0000 -filemethod 0
    File ::CE625370-D14A-4C57-A78E-70048B295EF3 -name footer.html -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates -size 154 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::2ACD57EA-ACDF-4B17-AD3B-7939E7ACB972 -name header.html -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates -size 1006 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::A43036E4-1273-4334-95B7-47F08270361A -name index.html -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates -size 206 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::F235D3AF-16F4-4404-B420-C39978064E32 -name Configure -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates/Configure -type dir -attributes 0000 -filemethod 0
    File ::91CFE8CF-18B4-48FB-8962-CD0BF9DCE45C -name index.html -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates/Configure -size 2384 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::BA8BCBB1-B398-4CCC-B633-D435D4827DE3 -name Scanner -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates/Scanner -type dir -attributes 0000 -filemethod 0
    File ::E2307C5E-F797-4D79-8EEC-86C8763FC314 -name exist.html -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates/Scanner -size 610 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::19A52047-38DB-443A-812B-776D42BCE8E1 -name index.html -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates/Scanner -size 5472 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::C3BBBCDE-E526-41A8-9333-EAD6CE0B87D0 -name uploadscan.html -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates/Scanner -size 954 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::767126FD-F554-4DF9-95D8-348478FBF39B -name uploadscantest.html -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Templates/Scanner -size 109651 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::FA071087-C684-4683-ADDA-A5DEC6F02065 -name Tesseract -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -type dir -attributes 0000 -filemethod 0
    File ::FFAD42A8-D0F7-488A-A982-D488E5FCE434 -name eng.DangAmbigs -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 431 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::385571C6-8B95-4FFC-8981-463F99ACFA45 -name eng.freq-dawg -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 672 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::5EAAB269-1892-4985-B469-373F25F138C3 -name eng.inttemp -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 862544 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::30B4D946-5D5F-4091-9F9D-7AA089441E8F -name eng.normproto -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 41109 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::1F428E11-9045-4E9A-973E-6CCCBCC2514A -name eng.pffmtable -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 701 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::0AE18FE3-2091-4E62-9BDF-37628D9AECD8 -name eng.unicharset -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 593 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::A2C24F30-5D6F-4EB1-8138-858F39F1F931 -name eng.user-words -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 8210 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::24C22317-803D-4C66-8744-94AD00641595 -name eng.word-dawg -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 809728 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::D4AB51B2-8C67-46F5-80FC-A11ACF0AA44A -name ita.DangAmbigs -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 431 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::4C3A7C02-F268-40D2-B753-7C126311D9AC -name ita.freq-dawg -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 904 -mtime 1290527992 -attributes 1000 -filemethod 0
    File ::1229264F-FFD4-43BA-B962-6D3C24752C58 -name ita.inttemp -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 918678 -mtime 1290527993 -attributes 1000 -filemethod 0
    File ::B1A5A69A-B986-4192-B640-442F46733BB5 -name ita.normproto -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 42511 -mtime 1290527993 -attributes 1000 -filemethod 0
    File ::980572F5-1D08-4138-A206-CECADF7C93E5 -name ita.pffmtable -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 797 -mtime 1290527993 -attributes 1000 -filemethod 0
    File ::9E770C40-88DA-4138-BFAC-8E87DF37D7AA -name ita.unicharset -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 671 -mtime 1290527993 -attributes 1000 -filemethod 0
    File ::DFCB6E8A-E556-4080-8ED3-45D8E85035E3 -name ita.user-words -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -mtime 1290527993 -attributes 1000 -filemethod 0
    File ::82726C80-A253-4E1F-AE69-191B0ED49A32 -name ita.word-dawg -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%>/Tesseract -size 735880 -mtime 1290527993 -attributes 1000 -filemethod 0
    File ::610EA755-039B-411B-A6D3-3A7E305DD8A4 -name padoclient.pdb -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 101888 -mtime 1290529388 -attributes 1000 -filemethod 0
    File ::162478F8-0019-4D14-8670-71858BCFF707 -name padoclient.exe.Config -parent DFAA5EA0-DA4B-4C08-80DA-B63633EB9772 -directory <%InstallDir%> -size 328 -mtime 1290529816 -attributes 1000 -filemethod 0

}
