using System;
using System.Configuration;
using System.IO;
using System.Net;

namespace padoclient
{

	public class Settings
	{
		private int _port = 80;
		private string _username = "admin";
		private string _password = "5f4dcc3b5aa765d61d8327deb882cf99";
		private int _updateFrequency = 180; // In seconds
		private IPAddress _ipaddress = IPAddress.Loopback;
		
		public Settings ()
		{
			password = ConfigurationManager.AppSettings["password"]; 
			username = ConfigurationManager.AppSettings["username"];
			try { 
				port = int.Parse(ConfigurationManager.AppSettings["port"]); 
			}
			catch(System.ArgumentNullException) { 
				// do nothing 
			}
			try {
				this.updateFrequency = int.Parse(ConfigurationManager.AppSettings["updateFrequency"]);
			}
			catch(System.ArgumentNullException) { // Default updateFrequency (seconds)
				this.updateFrequency = 3;
			}
			try { 
				ipaddress = IPAddress.Parse(ConfigurationManager.AppSettings["ipaddress"]); 
			}
			catch(System.ArgumentNullException) { 
				// do nothing 
			}
		}
		
	    public int port 
	    {
	      get { return _port; }
	      set { _port = value; }
	    }	
		
	    public string username 
	    {
	      get { return _username; }
	      set { _username = value; }
	    }	
		
	    public string password 
	    {
	      get { return _password; }
	      set { _password = value; }
	    }
		
	    public IPAddress ipaddress 
	    {
	      get { return _ipaddress; }
	      set { _ipaddress = value; }
	    }
		
	    public int updateFrequency 
	    {
	      get { return _updateFrequency; }
	      set { _updateFrequency = value; }
	    }
		
		// Change Application settings
		public void updateAppSettingsParam(System.Configuration.Configuration config,
			string pkey, string pvalue) {
			
			// Store settings.
			config.AppSettings.Settings.Remove(pkey);
			config.AppSettings.Settings.Add(pkey, pvalue.ToString());
		}
		
		public void Save() {
			// Get the current configuration file.
     		System.Configuration.Configuration config =
				ConfigurationManager.OpenExeConfiguration(
					ConfigurationUserLevel.None);

			// Make changes to the new configuration file. 
		    // This is to show that this file is the 
		    // one that is used.
		    string sectionName = "appSettings";
			
			// Get the AppSettings section.
			AppSettingsSection appSettingSection =
				(AppSettingsSection)config.GetSection(sectionName);
			
			// Change values in configuration file
			updateAppSettingsParam(config, "port", port.ToString());
			updateAppSettingsParam(config, "username", username.ToString());
			updateAppSettingsParam(config, "password", password.ToString());
			updateAppSettingsParam(config, "ipaddress", ipaddress.ToString());

			// Save changed settings
			config.Save(ConfigurationSaveMode.Modified);
			
			// Force a reload of the changed section.
			// This makes the new values available for reading.
			ConfigurationManager.RefreshSection(sectionName);
		}
		
	}
}

