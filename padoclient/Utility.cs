using System;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Security.Cryptography;

namespace padoclient
{
	public class Utility
	{
		public Utility ()
		{			
		}
		
	    public static string md5hash(string input)
	    {
	        // Create a new instance of the MD5CryptoServiceProvider object.
	        MD5 md5Hasher = MD5.Create();
	
	        // Convert the input string to a byte array and compute the hash.
	        byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
	
	        // Create a new Stringbuilder to collect the bytes
	        // and create a string.
	        StringBuilder sBuilder = new StringBuilder();
	
	        // Loop through each byte of the hashed data 
	        // and format each one as a hexadecimal string.
	        for (int i = 0; i < data.Length; i++)
	        {
	            sBuilder.Append(data[i].ToString("x2"));
	        }
	
	        // Return the hexadecimal string.
	        return sBuilder.ToString();
	    }
		
		public static Image ResizeImage(Image source, int width, int height)
		{
			// new image size
			Size size = new Size(width, height);

			int sourceWidth = source.Width;
			int sourceHeight = source.Height;
			
			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;
			
			nPercentW = ((float)size.Width / (float)sourceWidth);
			nPercentH = ((float)size.Height / (float)sourceHeight);
			
			if (nPercentH < nPercentW)
			  nPercent = nPercentH;
			else
			  nPercent = nPercentW;
			
			int destWidth = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);
			
			Bitmap b = new Bitmap(destWidth, destHeight);
			Graphics g = Graphics.FromImage((Image)b);
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;
			
			g.DrawImage(source, 0, 0, destWidth, destHeight);
			g.Dispose();
			
			return (Image)b;
		}
		
		///  FUNCTION Enquote Public Domain 2002 JSON.org
		///  @author JSON.org
		///  @version 0.1
		///  Ported to C# by Are Bjolseth, teleplan.no
		public static string Enquote(string s)
		{
			if (s == null || s.Length == 0)
			{
				return "\"\"";
			}
			char         c;
			int          i;
			int          len = s.Length;
			StringBuilder sb = new StringBuilder(len + 4);
			string       t;
		
			sb.Append('"');
			for (i = 0; i < len; i += 1)
			{
				c = s[i];
				if ((c == '\\') || (c == '"') || (c == '>'))
				{
					sb.Append('\\');
					sb.Append(c);
				}
				else if (c == '\b')
					sb.Append("\\b");
				else if (c == '\t')
					sb.Append("\\t");
				else if (c == '\n')
					sb.Append("\\n");
				else if (c == '\f')
					sb.Append("\\f");
				else if (c == '\r')
					sb.Append("\\r");
				else
				{
					if (c < ' ')
					{
						//t = "000" + Integer.toHexString(c);
						string tmp = new string(c,1);
						t = "000" + int.Parse(tmp,System.Globalization.NumberStyles.HexNumber);
						sb.Append("\\u" + t.Substring(t.Length - 4));
					}
					else
					{
						sb.Append(c);
					}
				}
			}
			sb.Append('"');
			return sb.ToString();
		}

		static public string Base64Encode(string data)
		{
		    try
		    {
		        byte[] encData_byte = new byte[data.Length];
		        encData_byte = System.Text.Encoding.UTF8.GetBytes(data);    
		        string encodedData = Convert.ToBase64String(encData_byte, 
				                                            Base64FormattingOptions.InsertLineBreaks);
		        return encodedData;
		    }
		    catch(Exception e)
		    {
		        throw new Exception("Error in base64Encode" + e.Message);
		    }
		}
		
		static public string Base64Encode(string filename, bool open)
		{
		    try
		    {
				// open stream
				FileStream handle = File.OpenRead(filename);
				
				// Create a byte array of file stream length
				byte[] encData_byte = new byte[handle.Length];
				
				//Read block of bytes from stream into the byte array
				handle.Read(encData_byte,0,System.Convert.ToInt32(handle.Length));
				
				//Close the File Stream
				handle.Close();
				   
		        string encodedData = Convert.ToBase64String(encData_byte, 
				                                            Base64FormattingOptions.InsertLineBreaks);
		        return encodedData;
		    }
		    catch(Exception e)
		    {
		        throw new Exception("Error in base64Encode" + e.Message);
		    }
		}
		
		static public string FileContent(string path)
		{
	        if (File.Exists(path))
	        {
		        using (FileStream handle = File.OpenRead(path))
		        {
					string content = "";
		            byte[] b = new byte[handle.Length];
		            UTF8Encoding temp = new UTF8Encoding(true);
		            while (handle.Read(b,0,b.Length) > 0)
		            {
		                // Console.WriteLine("UTF-8 content");
						content = temp.GetString(b);
		            }
					return content;
		        }
	        }
			else {
				throw new Exception("Not Found");
			}
			return null;
		}
		
		static public string ApplicationPath()
		{
			return System.AppDomain.CurrentDomain.BaseDirectory;
		}
		
		static public string NormalizePath(string relativepath)
		{
			return System.IO.Path.GetFullPath(Utility.ApplicationPath()+relativepath);
		}
		
	}
}

