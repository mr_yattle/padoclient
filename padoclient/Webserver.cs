using System;

// Web service
using System.Net;
using System.Net.Sockets;
using Kayak;
using Kayak.Framework;
using System.IO;
using System.Text;
using System.Reflection;
using LitJson;
using Kayak.Core;
using System.Collections.Generic;
using GenericOS.Component;
using WinOS.Component;

// Presentation
using System.Drawing;
using System.Drawing.Imaging;

// Stop process
using System.Diagnostics;

namespace padoclient
{
	
	public class Webserver
	{
		
		private Settings settings = null;
		
		/* Start webservice using system service (Unix) */
		public Webserver ()
		{
			// application settings
			settings = new Settings();
		}
		
		/* Start webservice using system service (Windows) */
		public Webserver (Settings _settings)
		{
			// store application settings passed by service
			settings = _settings;
		}
		
		/* Debug class */
		public static void _Main(string[] args) 
		{ 
			Console.WriteLine("start webserver ....");
			Webserver app = new Webserver();
			app.execute();
		}
		
		/* start service class */
		public void execute() 
		{ 
			try {
				this._execute();
			}
			catch(Exception ex) {
				Log.fatal(ex.Message);
			}
		}
		
		public void _execute() 
		{ 
            var server = new KayakServer(new IPEndPoint(settings.ipaddress, settings.port)); 
			
            var mm = new Type[] { typeof(PadocClientService) }.CreateMethodMap();
            var jm = new JsonMapper2();
            jm.AddDefaultInputConversions();
            jm.AddDefaultOutputConversions();

            server.RespondWith(new KayakFrameworkResponder(mm, jm));
		}
		
		public void stop()
		{
			Process.GetCurrentProcess().Close();
		}
		
	}
	
	public class MissingModule: Exception {
		public MissingModule() {}
   		public MissingModule(string message): base(message) {}
	}
	public class CannotLoadComponent: Exception {
		public CannotLoadComponent() {}
   		public CannotLoadComponent(string message): base(message) {}
	}
	public class NotSupportedOS: Exception {
		public NotSupportedOS() {}
   		public NotSupportedOS(string message): base(message) {}
	}
	
	public class OSDetect
	{
		private System.OperatingSystem info = System.Environment.OSVersion;
		
		public OSDetect ()
		{
		}
		
		public string ComponentPrefix() {
			if(info.ToString().IndexOf("Microsoft Windows NT") != -1) 
				return "WinOS";
			throw new NotSupportedOS("This kind of operating system/version is not currently supported");
		}
		
	}
	
    public class PadocClientService : KayakService
    {
		
        [Path("/")]
        public object Root()
        {
			return Page("index.html");
        }
		
		/*
        [Path("/index.html")]
        public FileInfo GetFile(string name)
        {
            return new FileInfo("index.html");
        }
        */
		
		public string GetFileContent(string path)
		{
	        if (File.Exists(path))
	        {
		        using (FileStream handle = File.OpenRead(path))
		        {
					string content = "";
		            byte[] b = new byte[handle.Length];
		            UTF8Encoding temp = new UTF8Encoding(true);
		            while (handle.Read(b,0,b.Length) > 0)
		            {
		                // Console.WriteLine("UTF-8 content");
						content = temp.GetString(b);
		            }
					return content;
		        }
	        }
			else {
				throw new Exception("Not Found");
			}
			return null;
		}
		
		public string normalizePath(string relativepath)
		{
			//return System.IO.Path.GetFullPath(Environment.CurrentDirectory+relativepath);
			// Ifservice
			return System.IO.Path.GetFullPath(Utility.ApplicationPath()+relativepath);
		}
		
		
		private Dictionary<string, string> httpDigestParse(string authorization)
		{
		    // protect against missing data
			// array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 
			//'username'=>1, 'uri'=>1, 'response'=>1);
			string[] neededParts = new string[]{"nonce", "nc", 
				"cnonce", "qop", "username", "uri", "response"}; 
	
			Dictionary<string, string> data = new Dictionary<string, string>();
			Dictionary<string, string> elements = new Dictionary<string, string>();
			string[] parts = authorization.Split(',');
			
			// Order keys
			foreach(string part in parts) {
				string[] nameValue = part.Split('=');
				string name = nameValue[0].Trim();
				elements.Add(name, nameValue[1].Trim().Trim('"').Trim('\''));				
			}
			
			// match exact keys
			foreach(string _name in neededParts) {
				if(elements.ContainsKey(_name)) {
					data.Add(_name, elements[_name]);
					elements.Remove(_name);
				}
			}
			
			// match similar keys
			foreach(var pair in elements) {
				foreach(string _name in neededParts) {
					if(pair.Key.IndexOf(_name) != -1) {
						if(!data.ContainsKey(_name)) {
							data.Add(_name, pair.Value);
							elements.Remove(_name);
						}
					}
				}
			}
			
			return data;
		}
		
		
		/* Useful for http auth */
		private string Md5(string normalString)
		{
			return Utility.md5hash(normalString);
		}
		

		
		public object ChallengeAuthentication(string realm, BufferedResponse response) {
			string nonce = System.Guid.NewGuid().ToString();
			string opaque = Md5(realm);
			response.Status = "401 Unauthorized";
			response.Headers["WWW-Authenticate"] = String.Format("Digest realm=\"{0}\"," +
				"qop=\"auth\"," +
				"nonce=\"{1}\"," +
				"opaque=\"{2}\"", realm, nonce, opaque);
			return response;
		}
		
		/* authenitcate users to use specific kind of page */
		public object auth(string realm) {
			Console.WriteLine("AUTH requred for page: {0}", this.Request.RequestUri);
			Dictionary<string, string> users = new Dictionary<string, string>();
			//users.Add("admin", "password");
			Settings settings = new Settings();
			users.Add(settings.username, settings.password);
			var response = new BufferedResponse();
			try {
				if(!this.Request.Headers.ContainsKey("Authorization")) {
					this.ChallengeAuthentication(realm, response);
					response.Add("<h1>Unauthorized</h1><p>Authentication " +
							" required");
					return response;
				}
				else {
					Dictionary<string, string> data = 
						this.httpDigestParse(this.Request.Headers["Authorization"]);
					
					/*
					foreach(var pair in this.Request.Headers) {
						Console.WriteLine("{0}:{1}", pair.Key, pair.Value);
					}
					
					foreach(var pair in data) {
						Console.WriteLine("{0}:{1}", pair.Key, pair.Value);
					}
					*/
					
					if(!data.ContainsKey("username")) {
						this.ChallengeAuthentication(realm, response);
						response.Add("<h1>Unauthorized</h1><p>Check " +
							"if your browser support Digest Auth");
						return response;
					}
					
					if(data["username"].Equals("") || data["username"] == null) {
						this.ChallengeAuthentication(realm, response);
						response.Add("<h1>Unauthorized</h1><p>Cannot " +
							"use empty username");
						return response;
					}
					
					// Check password
					try {
						string A1 = data["username"]+
							":"+realm+
						    ":"+users[data["username"].ToString()];
						string A2 = this.Request.Verb+":"+data["uri"];
						string A3 = Md5(A1)+":"+data["nonce"]+
						   	":"+data["nc"]+":"+data["cnonce"]+
						    ":"+data["qop"]+":"+Md5(A2);
						string valid_response = Md5(A3);
						
						if (data["response"] != valid_response) {
							this.ChallengeAuthentication(realm, response);
							response.Add("<h1>Unauthorized</h1><p>Check " +
								"you credentials");
							return response;
						}
					}
					catch(KeyNotFoundException) {
						this.ChallengeAuthentication(realm, response);
						response.Add("<h1>Unauthorized</h1><p>Check " +
								"you credentials</p>");
						return response;
					}
					catch(Exception ex) {
						this.ChallengeAuthentication(realm, response);
						response.Add("<h1>Unauthorized</h1><p>Check " +
								"you credentials</p>" +
								"<pre>"+ex.ToString()+"</pre>");
						return response;
					}
					
				}
					
			}
			catch(Exception ex) {
				response.Add("<h1>Unauthorized</h1><p>Some " +
							"errors occurs in auth process</p>" +
							"<pre>"+ex.Message+"</pre>");
				return response;
			}
			return null;
		}
		
		// Configuration index
		[Path("/Configure/{name}")]
        public object ConfigurePages(string name)
        {
			// Auth index.html
			if(name == "index.html") {
				string realm = "P@docClient";
				object response = this.auth(realm);
				if(response != null) {
	            	return response;
				}
			}
			return Page("Configure", name);
        }
		
        [Path("/homepage.html")]
        public object Homepage()
        {
			return Page("index.html");
        }
		
		/* With PATH() we organize files templates on disk */
		[Verb("GET")] 
        [Path("/{name}")]
        public object Page(string name)
        {
			/*
			We can handle 3 types of extension: 
			.html (static page)
			.css (css page)
			.image (gif, jpeg, png)
			.json (json content)
			.do (device command)	
			*/
			// Find http path file extension name
			string[] http_path = name.Split('.');
			string extension = http_path[http_path.Length-1];
			var response = new BufferedResponse();
			
			// This version of kajak does not fully support POST request 
			// (need to change version)
			if(this.Request.Verb == "POST") {
	            response.Status = "500 Internal Server Error";
	            response.Headers["Content-Type"] = "text/html";
	            response.Add("<h1>Internal Server Error</h1><p>Actually could not handle POST request");
		        return response;
			}
			
			
			switch (extension)
			{
			    case "html": 
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseTemplate(name);
			        break;
				case "js": 
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseJavascript(name);
			        break;
				case "json":
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseJson(name);
			        break;
				case "do":
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseExecute(name);
			        break;
				case "cgi":
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseExecute(name);
			        break;
			    case "css": 
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseCss(name);
			        break;
				case "jpg": 
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseImage(name, null);
			        break;
				case "gif": 
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseImage(name, null);
			        break;
				case "png": 
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseImage(name, null);
			        break;
				case "ico": 
					//Console.WriteLine("Render : "+extension.ToUpper());
			        response = (BufferedResponse) ResponseImage(name, "image/x-icon");
			        break;
			    default:
		            response.Status = "500 Internal Server Error";
		            response.Headers["Content-Type"] = "text/html";
		            response.Add("<h1>Internal Server Error</h1><p>This kind of extension ["+extension+"] currently is not supported");
			        break;
			}
			return response;
        }
		
		[Verb("GET")] 
		[Path("/{device}/{name}")]
        public object Page(string device, string name)
        {
            return Page(device+"/"+name);
        }
		
		// Preview image
		[Verb("GET")] 
		[Path("/Data/{device}/{name}")]
        public object PreviewImage(string device, string name)
        {
			// also download file
			IDictionary<string, string> parameters = this.Request.GetQueryString();
			var response = new BufferedResponse();
			try {
				string path = "/Data/"+device+"/";
				if(parameters.ContainsKey("download")) {
			        response = (BufferedResponse) ResponseDownload(path, name, null);
				}
				else {
			        response = (BufferedResponse) ResponsePreviewImage(path, name, null);
				}
			}
			catch(Exception) {
	            response.Status = "500 Internal Server Error";
	            response.Headers["Content-Type"] = "text/html";
	            response.Add("<h1>Internal Server Error</h1><p>Cannot retrieve image");
			}
			return response;
        }
		
		private string ReplaceVars(string template) {
			/*
			Settings settings = new Settings();
			Console.WriteLine(this.Request.RequestUri);
			string content = template.Replace("$localhost", settings.host+":"+settings.port);
			*/
			return template;
		}
		
		private string absolutize(string name) {
			/*
			Get css, images, js from unique uri
			- es(http//localhost/jquery.js even if http//localhost/device/other/jquery.js)
			*/
			string[] http_path = name.Split('/');
			string filename = http_path[http_path.Length-1];
			return filename;
			
		}
		
		private object ResponseTemplate(string name)
        {
			// Se si tratta di un template .html
			string f =  this.GetFileContent(this.normalizePath("/Templates/header.html"));
			f +=  this.GetFileContent(this.normalizePath("/Templates/"+name));
			f +=  this.GetFileContent(this.normalizePath("/Templates/footer.html"));
			var response = new BufferedResponse();
			response.Headers["Content-Type"] = "text/html; charset=utf-8";
            response.Add(ReplaceVars(f));
			//response.Add(this.normalizePath("/Templates/"+name));
			return response;
        }	
		
		private object ResponseCss(string name)
        {
			// Se si tratta di un template .html
			string f =  this.GetFileContent(this.normalizePath("/Css/"+this.absolutize(name)));
			var response = new BufferedResponse();
			response.Headers["Content-Type"] = "text/css; charset=utf-8";
            response.Add(ReplaceVars(f));
			return response;
        }	
		
		private object ResponseJavascript(string name)
        {
			// Se si tratta di un template .html
			string f =  this.GetFileContent(this.normalizePath("/Javascript/"+this.absolutize(name)));
			var response = new BufferedResponse();
			response.Headers["Content-Type"] = "text/javascript; charset=utf-8";
            response.Add(ReplaceVars(f));
			return response;
        }
		
		private object ResponseImage(string name, string content_type)
        {
			// Se si tratta di un template .html
			FileStream handle = File.OpenRead(this.normalizePath("/Images/"+this.absolutize(name)));
			byte[] binaryBytes = new byte[handle.Length];
			
			// Read data (do not encode to preseve binary content)
			handle.Read(binaryBytes, 0, binaryBytes.Length);

			if (content_type == null) {
				string[] name_parts = name.Split('.');
				content_type = "image/"+name_parts[name_parts.Length-1];
			} 
			var response = new BufferedResponse();
			response.Headers["Content-Type"] = content_type;
            response.Add(binaryBytes);
			return response;
        }
		
		private object ResponseDownload(string path, string name, string content_type)
        {
			// Se si tratta di un template .html
			FileStream handle = File.OpenRead(this.normalizePath(path+this.absolutize(name)));
			byte[] binaryBytes = new byte[handle.Length];
			
			// Read data (do not encode to preseve binary content)
			handle.Read(binaryBytes, 0, binaryBytes.Length);
			var response = new BufferedResponse();
			response.Headers["Content-Type"] = "application/force-download";
			response.Headers["Content-Disposition"] = "attachment; filename=\""+
				Path.GetFileName( this.normalizePath(path+this.absolutize(name)) );
			response.Headers["Cache-Control"] = "no-cache";
			response.Headers["Pragma"] = "no-cache";
			response.Headers["Expires"] = "Fri, 01 Jan 2010 05:00:00 GMT";
            response.Add(binaryBytes);
			return response;
        }
		
		
		private object ResponsePreviewImage(string path, string name, string content_type)
        {
			try {
				// Se si tratta di un template .html
				// FileStream handle = File.OpenRead(this.normalizePath(path+this.absolutize(name)));
				Image sourceImage = Image.FromFile(this.normalizePath(path+this.absolutize(name)));
				
				// Resize image
				IDictionary<string, string> parameters = this.Request.GetQueryString();
				int width = 0;
				int height = 0;
				if(parameters.ContainsKey("width"))
					width = int.Parse(parameters["width"]);
				if(parameters.ContainsKey("height"))
					height = int.Parse(parameters["height"]);
				
				
				if(height > 0 || width > 0) {
					if(height == width) {
						if(sourceImage.Width > sourceImage.Height) {
							height = 0;
						}
						else {
							width = 0;
						}
					}
					if(height == 0) {
						float percent = 0;
						percent = ((float)sourceImage.Width / (float)width);
						height = (int)((float)sourceImage.Height * percent);
					}
					if(width == 0) {
						float percent = 0;
						percent = ((float)sourceImage.Height / (float)height);
						width = (int)((float)sourceImage.Width * percent);
					}
					sourceImage = Utility.ResizeImage(sourceImage, width, height);
				}
					
				// put image in memory
				MemoryStream memory = new MemoryStream();
				sourceImage.Save(memory, ImageFormat.Jpeg);
				
				// byte[] binaryBytes = new byte[handle.Length];
				byte[] binaryBytes = memory.ToArray();
				
				// Read data (do not encode to preseve binary content)
				//handle.Read(binaryBytes, 0, binaryBytes.Length);

				if (content_type == null) {
					string[] name_parts = name.Split('.');
					content_type = "image/"+name_parts[name_parts.Length-1];
				} 
				var response = new BufferedResponse();
				response.Headers["Content-Type"] = content_type;
	            response.Add(binaryBytes);
				//response.Add(ms);
				return response;
			}
			catch(Exception ex) {
	            var response = new BufferedResponse();
	            response.Status = "500 Internal Server Error";
	            response.Headers["Content-Type"] = "text/html";
	            response.Add("<h1>Internal Server Error</h1><pre>"+ex.ToString()+"</pre>");
	            return response;
			}
        }
		
		private Type LoadComponent(string name) {
			try {
				string objectName = "GenericOS.Component."+name;
				Type magicType = Type.GetType(objectName, true);
				return magicType;
			}
			catch(TypeLoadException) {
				try {
					OSDetect os = new OSDetect();
					string objectName = os.ComponentPrefix()+".Component"+"."+name;
					Type magicType = Type.GetType(objectName, true);
					return magicType;
				}
				catch(TypeLoadException ex) {
					throw new CannotLoadComponent("Cannot load component "+name+":"+ex.ToString());
				}
			}
		}
		
		private object traverse(string name) {
			// Deduce Component/Method from name (Configuration/look.json)
			string[] http_path = name.Split('/');
			string objectName = http_path[0];
			
			string page_path = http_path[http_path.Length-1];
			string[] traverse_path = page_path.Split('.');
			string methodName = traverse_path[0];
			
			Type magicType = LoadComponent(objectName); 
			//Type.GetType(objectName, true);
			ConstructorInfo magicConstructor = magicType.GetConstructor(Type.EmptyTypes);
			try {
				object component = magicConstructor.Invoke(new object[]{});
				MethodInfo magicMethod = magicType.GetMethod(methodName);
				IDictionary<string, string> queryString = this.Request.GetQueryString();
				try {
					object magicValue = magicMethod.Invoke(component, new object[]{queryString});
					return magicValue;
				}
				catch(NullReferenceException ex) {
					throw new CannotLoadComponent("Missing method "+methodName+":"+ex.ToString());
				}
			}
			catch(NullReferenceException ex) {
				throw new CannotLoadComponent("Cannot load component "+objectName+":"+ex.ToString());
			}
			
			/*
			// Dynamic page (user params)
			IDictionary<string, string> parameters = this.Request.GetQueryString();

			foreach(string key in parameters.Keys) {
				Console.WriteLine("Key : "+key+" Value: "+parameters[key]);
			}
			*/
			return null;
		}
		
		private object ResponseJson(string name)
        {
			try {
				object magicValue = traverse(name);
				var response = new BufferedResponse();
				response.Headers["Content-Type"] = "application/javascript; charset=utf-8";
				response.Add(ReplaceVars((string) magicValue));
				return response;
			}
			catch(CannotLoadComponent ex) {
	            var response = new BufferedResponse();
	            response.Status = "500  Internal Server Error";
	            response.Headers["Content-Type"] = "text/html";
	            response.Add("<h1>Internal Server Error</h1><pre>"+ex.ToString());
	            return response;
			}
			catch(Exception ex) {
	            var response = new BufferedResponse();
	            response.Status = "500  Internal Server Error";
	            response.Headers["Content-Type"] = "text/html";
	            response.Add("<h1>Internal Server Error</h1><p>"+ex.ToString());
	            return response;
			}
        }
		
		private object ResponseExecute(string name)
        {
			/*
			 * Execute command response return JSON
			 * Statuses:
			 * 	0 = yes
			 * 	1 = no
			 * 	2 = errors
			 * {status:1, errors:[], messages:[]}
			 * */
			
			try {
				object magicValue = traverse(name);
				var response = new BufferedResponse();
				if(this.Request.GetQueryString().ContainsKey("content-type")) {
					response.Headers["Content-Type"] = 
						this.Request.GetQueryString()["content-type"]+"; charset=utf-8";
				}
				else {
					response.Headers["Content-Type"] = "application/javascript; charset=utf-8";
				}
				response.Add(ReplaceVars((string) magicValue));
				return response;
			}
			catch(CannotLoadComponent ex) {
	            var response = new BufferedResponse();
	            response.Status = "500  Internal Server Error";
	            response.Headers["Content-Type"] = "text/html";
	            response.Add("<h1>Internal Server Error</h1><pre>"+ex.ToString());
	            return response;
			}
			catch(Exception ex) {
	            var response = new BufferedResponse();
	            response.Status = "500  Internal Server Error";
	            response.Headers["Content-Type"] = "text/html";
	            response.Add("<h1>Internal Server Error</h1><p>"+ex.ToString());
	            return response;
			}
			/*
			catch(Exception ex) {
	            var response = new BufferedResponse();
	            response.Status = "404 Not Found";
	            response.Headers["Content-Type"] = "text/html";
	            response.Add("<h1>Not Found</h1><p>"+ex.ToString());
	            return response;
			}
			*/
        }

		 
    }
	
	
}

