using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Threading;

namespace padoclient
{
	public class Ocr
	{
		public Ocr ()
		{
		}
		
		/* Get words indexing in xml format */
		static public string ToXml(string filepath) {
			string langs = System.IO.Path.GetFullPath(Utility.ApplicationPath()+"/Tesseract");
			return ToXml(filepath, langs, "ita");
		}
		
		static public string ToXml(string filepath, string langs, string lang) {
			
            using (Bitmap bmp = new Bitmap(filepath))
            {
                tessnet2.Tesseract tessocr = new tessnet2.Tesseract();
                tessocr.Init(langs, lang, false);
				string buffer = System.IO.Path.GetFullPath(Utility.ApplicationPath()+"/Data/Scanner/"
				                                           +Guid.NewGuid().ToString() + ".tiff");
                tessocr.GetThresholdedImage(bmp, Rectangle.Empty).Save(buffer);
				List<tessnet2.Word> result = tessocr.DoOCR(bmp, Rectangle.Empty);
				
				string xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
				xml += "<ocr>\n";
				foreach (tessnet2.Word word in result) {
					xml += String.Format(" <word confidence=\"{0}\">{1}</word>\n",
						word.Confidence, word.Text);
				}
				xml += "</ocr>";
				return xml;
            }
			
		}
		
	}
}

