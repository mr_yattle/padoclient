using System;
using System.IO;
using System.Collections.Generic;

namespace padoclient
{
	public class Metadata
	{
		private Dictionary<string, string> _filespath;
		
		public Metadata ()
		{
			_filespath = new Dictionary<string, string>();
		}
		
		public void AddFile(string type, string filename) {
			_filespath.Add(type, Path.GetFileName(filename));
		}
		
		public string ToXml() {
			string xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
			xml += "<metadata>\n";
			foreach (var pair in _filespath) {
				xml += String.Format(" <file type=\"{0}\" filename=\"{1}\" />\n",
					pair.Key, pair.Value);
			}
			xml += "</metadata>";
			return xml;
		}
		
	}
}

