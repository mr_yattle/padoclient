using System;
using win32scanner;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using TwainDotNet;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using padoclient;

namespace WinOS.Component
{
	
	public class WebTwainScanner: TwainScanner
	{
		private Twain _hwScanner;
		private MyWinFormsWindowMessageHook _windowResource;
		
		public WebTwainScanner (MyWinFormsWindowMessageHook windowResource)
		{
			_WebTwainScanner(windowResource);
		}
		
		public void _WebTwainScanner (MyWinFormsWindowMessageHook windowResource)
		{
			//_windowResource = windowResource;
			_windowResource = new MyWinFormsWindowMessageHook(new Form());
			_hwScanner = new Twain(_windowResource);
		}
	}
	
	public class Scanner
	{
		
		public MyWinFormsWindowMessageHook windowResource = null;
		public Scanner ()
		{
			windowResource = new MyWinFormsWindowMessageHook(new Form());
		}
		
		/* get all info about scan session */
		public string scaninfo(IDictionary<string, string> queryString) {
			/*
			 * {
			 * 	"jpgs":["Page1.jpg", "Page2.jpg"]
			 *  "tiff":{
			 * 		"filename":"document-8e84e398-2546-4808-87ac-e866c35b3658.tiff"
			 * 		"encoding":"base64",
			 * 		"file":"$encodestring64data"}
			 * } 
			 */
			
			// Scan dir
			string saveDir = Utility.ApplicationPath()+"/Data/Scanner";
			
			// All jpgs generated for preview
			string jpgs = "";
			List<string> previews = new List<string>();
			foreach(string f in System.IO.Directory.GetFiles(System.IO.Path.GetFullPath(saveDir)))
			{
				if(f.Contains(".jpg")) {
					previews.Add(String.Format("\"{0}\"", 
						Path.GetFileName(f)));
				}
			}
			
			// All tiff generated to download and upload
			string tiff = "{"+String.Format("\"filename\":\"\", \"encoding\": \"base64\", \"file\": \"\"")+"}";
			string pdf = "{"+String.Format("\"filename\":\"\"")+"}";
			foreach(string f in System.IO.Directory.GetFiles(System.IO.Path.GetFullPath(saveDir)))
				{
				if(f.Contains(".tiff")) {
					string content = "";
					if(queryString.ContainsKey("data"))
						content = Utility.Base64Encode(Utility.FileContent(f));
					tiff = "{"+String.Format("\"filename\":\"{0}\", \"encoding\": \"base64\", \"file\": {1}", 
							Path.GetFileName(f), Utility.Enquote(content))
						+"}";
				}
				if(f.Contains(".pdf")) {
					pdf = "{"+String.Format("\"filename\":\"{0}\"", Path.GetFileName(f))+"}";
				}
			}
			
			jpgs = string.Join(",", previews.ToArray());
			string json_response = String.Format("\"jpgs\":[{0}], \"tiff\":{1}, \"pdf\":{2}", 
			                                     jpgs, tiff, pdf);
			return "{"+json_response+"}";
		}
		
		/* get json string reading settings */
		public string retrieve(IDictionary<string, string> queryString) {
			/*
			 * {
			 * 	"models":["fj-xyz", "hp-xyz"],
			 *  "dpi":["75", "150", "300"],
			 *  "formats":["A4", "A3"],
			 *  "orientations":["P", "L"]
			 * } 
			 */
			TwainScanner scanner = new TwainScanner(true);
			IScanSettings settings = new IScanSettings();
			
			// Avaible scanners
			string models = "";
			List<string> avaibles = (List<string>) scanner.list();
			List<string> scanners = new List<string>();
			int selected = 0;
			if(avaibles != null) {
				foreach(string scannerName in avaibles) {
					selected = 0;
					if(scannerName.ToString() == scanner.scannerName().ToString())
						selected = 1;
					scanners.Add("{"+String.Format("\"name\":\"{0}\", \"selected\":\"{1}\"", 
						scannerName, selected.ToString())+"}");
				}
				models = string.Join(",", scanners.ToArray());
			}	
			
			// Avaible dpi
			string dpis = "";
			List<string> resolutions = new List<string>();
			if(settings.dpis().Keys.Count > 0) {
				foreach(string dpi in settings.dpis().Keys) {
					resolutions.Add(String.Format("\"{0}\"", dpi));
				}
				dpis = string.Join(",", resolutions.ToArray());
			}
			
			// Avaible page format (A4, A3)
			string formats = "";
			List<string> pageFormats = new List<string>();
			if(settings.formats().Keys.Count > 0) {
				foreach(string format in settings.formats().Keys) {
					pageFormats.Add(String.Format("\"{0}\"", format));
				}
				formats = string.Join(",", pageFormats.ToArray());
			}
			
			// Avaible page orientations (Portrait, Landscape)
			string orientations = "";
			List<string> pageOrientations = new List<string>();
			if(settings.orientations().Keys.Count > 0) {
				foreach(string orientation in settings.orientations().Keys) {
					pageOrientations.Add(String.Format("\"{0}\"", orientation));
				}
				orientations = string.Join(",", pageOrientations.ToArray());
			}
			
			string json_response = String.Format("\"models\":[{0}], " +
				"\"dpi\":[{1}], " +
				"\"formats\":[{2}]," +
				"\"orientations\":[{3}]", models, dpis, formats, orientations);
			return "{"+json_response+"}";
		}
		
		/* get json string reading settings */
		public string change(IDictionary<string, string> queryString) {
			int status = 0;
			string errors = "";
			string messages = "";
			try {
				TwainScanner scanner = new TwainScanner(true);
				scanner.choose();
				status = 1;
			}
			catch(Exception ex) {
				errors = String.Format("\"{0}\"", ex.Message.ToString());
				status = 2;
			}
			string json_response = String.Format("\"status\":\"{0}\", " +
				"\"errors\":[{1}], " +
				"\"messages\":[{2}]", status.ToString()
			    	, errors.ToString()
			        , messages.ToString());
			return "{"+json_response+"}";
		}
		
		/* get json string reading settings */
		public string scan(IDictionary<string, string> queryString) {
			int status = 0;
			string errors = "";
			string messages = "";
			try {
				if(this._scan(queryString)) {
					messages = "\"Image acquisition completed\"";
					status = 1;
				}
				else {
					errors = "\"Image acquisition failed\"";
					status = 2;
				}
			}
			catch(Exception ex) {
				errors = String.Format("\"{0}\"", ex.Message);
				status = 2;
			}
			string json_response = String.Format("\"status\":\"{0}\", " +
				"\"errors\":[{1}], " +
				"\"messages\":[{2}]", status.ToString()
			    	, errors.ToString()
			        , messages.ToString());
			return "{"+json_response+"}";
		}
		
		/* get dinamic html content and use browser to send data */
		public string uploadscan(IDictionary<string, string> queryString) {
			
			// Acquire documents
			if(!this._scan(queryString)) {
				throw new Exception("Acquisition Failed!");
			}
			
			// Scan dir
			string saveDir = Utility.ApplicationPath()+"/Data/Scanner";
			
			// template
			string template =  Utility.FileContent(Utility.NormalizePath("/Templates/Scanner/uploadscan.html"));
			string content = null;
			string filename = null;
			foreach(string f in System.IO.Directory.GetFiles(System.IO.Path.GetFullPath(saveDir)))
			{
				if(f.Contains(".tar.gz")) {
					//content = Utility.Base64Encode(Utility.FileContent(f));
					content = Utility.Base64Encode(f, true); // binary convert
					filename = Path.GetFileName(f);
				}
			}
			
			// If content is not missing ...
			if(content == null) {
				throw new Exception("Missing document!");
			}
			
			/*
			if(!queryString.ContainsKey("protocol"))
				throw new Exception("Missing network protocol (es:http, https)!");
			*/
			
			if(!queryString.ContainsKey("page"))
				throw new Exception("Missing page reference (es:$hot/page.html)!");
			
			/*
			string uploadpage = String.Format("{0}://{1}", 
			                                  queryString["protocol"], queryString["page"]);
			*/
			
			string uploadpage = queryString["page"];
			
			template = template.Replace("${uploadpage}", uploadpage);
			template = template.Replace("${filename}", filename);
			template = template.Replace("${content}", content);
			return template;
		}
		
		
		private bool _scan(IDictionary<string, string> queryString) {
			TwainScanner scanner = new TwainScanner(true);
			IScanSettings settings = new IScanSettings();
			PageFormat format = PageFormat.A4;
			if(queryString.ContainsKey("page-format")) {
				settings.paperFormat(queryString["page-format"]);
				switch(queryString["page-format"]) {
					case "A4":
						format = PageFormat.A4;
						break;
					case "A3":
						format = PageFormat.A3;
						break;
					default:
						format = PageFormat.A4;
						break;
				}
			}
			if(queryString.ContainsKey("page-orientation"))
				settings.orientation(queryString["page-orientation"]);
			if(queryString.ContainsKey("dpi"))
				settings.dpi(queryString["dpi"]);
			if(queryString.ContainsKey("colorset")) {
				switch(queryString["colorset"]) {
					case "c":
						settings.color();
						break;
					case "g":
						settings.grayscale();
						break;
					case "b":
						settings.blackandwhite();
						break;
					default:
						settings.grayscale();
						break;
				}
			}
			if(queryString.ContainsKey("adf"))
				settings.UseDuplex = true;
			if(queryString.ContainsKey("page-duplex"))
				settings.UseDuplex = true;
			if(queryString.ContainsKey("adf"))
				settings.UseDocumentFeeder = true;
			if(queryString.ContainsKey("gui"))
				settings.ShowTwainUI = true;
			
			// TWAIN timeout
			int timeout = 60;
			if(queryString.ContainsKey("timeout"))
				timeout = int.Parse(queryString["timeout"]);
			
			// Min 15 sec
			if(timeout < 16)
				throw new Exception("Too fast timeout to execute TWAIN");
			
			
			// also can choose TWAIN scanner to use
			if(queryString.ContainsKey("model")) {
				// Console.WriteLine(queryString["model"]);
				// scanner.chooseBeforeScan(queryString["model"]);
			}

			TwainScannerThread scannerThread = new TwainScannerThread();
			IList<Image> images = scannerThread.Scan(windowResource, 
			                                        	settings, timeout);
			if(images != null) {
				if(images.Count > 0) {
					save(images, format);
					return true;
				}
			}
			else {
				throw new Exception("Acquisition failed");
			}
			return false;
		}
		
		/* save images in Data dir */
	    private void save (IList<Image> images, PageFormat format)
	    {			
			string timestamp = DateTime.Now.ToString();
			timestamp = timestamp.Replace("/", "-");
			string saveDir = Utility.ApplicationPath()+"/Data";
			if(!Directory.Exists(System.IO.Path.GetFullPath(saveDir))) {
				// Create Data folder
        		System.IO.Directory.CreateDirectory(System.IO.Path.GetFullPath(saveDir));
			}
			saveDir = Utility.ApplicationPath()+"/Data/Scanner";
			if(!Directory.Exists(System.IO.Path.GetFullPath(saveDir))) {
				// Create Data folder
        		System.IO.Directory.CreateDirectory(System.IO.Path.GetFullPath(saveDir));
			}
			
			// GC Files
			foreach(string f in System.IO.Directory.GetFiles(System.IO.Path.GetFullPath(saveDir)))
			{
				System.IO.File.Delete(f);
			}
			
			/*
			int counter = 0;
			foreach(Image image in images){
				counter++;
				string savepath = System.IO.Path.GetFullPath(saveDir+"/"+timestamp+"-"+counter.ToString()+".tiff");
				image.Save(savepath, System.Drawing.Imaging.ImageFormat.Tiff);
			}
			*/
			
			if(images.Count > 0) {
				// Pages for web preview
				CreateWebPages(saveDir, images);
				// SAVE Multipage TIFF
				string tiffpath = CreateTiffDocument(saveDir, images);
				// SAVE Multipage PDF
				string pdfpath = CreatePdfDocument(saveDir, images, format);
				// Create OCR file
				string ocrpath = CreateOcr(saveDir, tiffpath);
				
				// Create Gzip archive
				string uid = System.Guid.NewGuid().ToString();
				string gzippath = System.IO.Path.GetFullPath(saveDir+"/gzip-"+uid+".tar.gz");
				
				// Create metadata.xml
				Metadata meta = new Metadata();
				meta.AddFile("pdf", pdfpath);
				meta.AddFile("tiff", tiffpath);
				meta.AddFile("ocr", ocrpath);
				string metapath = CreateMetadata(saveDir, meta);
				
				Compress archive = new Compress();
				archive.AddFile(pdfpath);
				archive.AddFile(tiffpath);
				archive.AddFile(ocrpath);
				archive.AddFile(metapath);
				archive.Gzip(gzippath); // Compress content and close Gzip file
			}
	    }
		
		private string CreateMetadata(string saveDir, Metadata meta) {
			string savepath = System.IO.Path.GetFullPath(saveDir+"/metadata.xml");
			// create a writer and open the file
            TextWriter tw = new StreamWriter(savepath);

			// write ocr xml result
			tw.Write(meta.ToXml());

            // close the stream
            tw.Close();	
			
			// Ocr filepath
			return savepath;
		}
		
		private string CreateOcr(string saveDir, string tiffile) {
			string uid = System.Guid.NewGuid().ToString();
			string savepath = System.IO.Path.GetFullPath(saveDir+"/ocr-"+uid+".xml");
			// create a writer and open the file
            TextWriter tw = new StreamWriter(savepath);

			// write ocr xml result
			tw.Write(Ocr.ToXml(tiffile));

            // close the stream
            tw.Close();	
			
			// Ocr filepath
			return savepath;
		}
		
		private void CreateWebPages(string saveDir, IList<Image> images) {
			int counter = 0;
			foreach(Image image in images){
				counter++;
				string uid = System.Guid.NewGuid().ToString();
				string savepath = System.IO.Path.GetFullPath(saveDir+"/Page"+counter.ToString()+"-"+uid+".jpg");
				image.Save(savepath, System.Drawing.Imaging.ImageFormat.Jpeg);
			}
		}
		
		private string CreateTiffDocument(string saveDir, IList<Image> images) {
			Image multipage = null;
			string uid = System.Guid.NewGuid().ToString();
			string savepath = System.IO.Path.GetFullPath(saveDir+"/document-"+uid+".tiff");
			Encoder enc=Encoder.SaveFlag;
			ImageCodecInfo info=null;
			foreach(ImageCodecInfo ice in ImageCodecInfo.GetImageEncoders())
				if(ice.MimeType=="image/tiff")
					info=ice;
			EncoderParameters ep=new EncoderParameters(1);
			ep.Param[0]=new EncoderParameter(enc,(long)EncoderValue.MultiFrame);
			
			// Add frames to file
			foreach(Image image in images){
				if(multipage == null) {
					multipage = image;
					multipage.Save(savepath,info,ep);
					// Add others
					ep.Param[0]=new EncoderParameter(enc,(long)EncoderValue.FrameDimensionPage);
				}
				else {
					multipage.SaveAdd(image, ep);
				}
			}
			// Close file
			ep.Param[0]=new EncoderParameter(enc,(long)EncoderValue.Flush);
			multipage.SaveAdd(ep);
			return savepath;
		}
		
		private string CreatePdfDocument(string saveDir, IList<Image> images, PageFormat format) {
			string uid = System.Guid.NewGuid().ToString();
			string savepath = System.IO.Path.GetFullPath(saveDir+"/document-"+uid+".pdf");
			// Add frames to file
			Pdf document = new Pdf();
			foreach(Image image in images){
				document.AddPage(image, format);
			}
			document.Save(savepath);
			return savepath;
		}
		
	}
	
	public class TwainScannerWork
	{
		public List<Exception> errors = new List<Exception>();
		public IScanSettings settings = null;
		public MyWinFormsWindowMessageHook windowResource = null;
		public WebTwainScanner scanner = null;
		private IList<Image> images = null;
	    // This method will be called when the thread is started.
	    public void DoWork()
	    {
			padoclient.Log.info("worker thread: scanning...");
			Console.WriteLine("worker thread: scanning...");
			try {
				// Set device lock to prevent concurrent access
				// to device
				DeviceLock();
				padoclient.Log.info("scan: init...");
				Console.WriteLine("scan: init...");
				scanner = new WebTwainScanner(windowResource);
				padoclient.Log.info("scan: start...");
				Console.WriteLine("scan: start...");
				scanner.scan(settings, false);
				images = scanner.Images;
			}
			catch(Exception ex) {
				errors.Add(ex);
			}
	    }
		
		public TwainScannerWork(MyWinFormsWindowMessageHook _windowResource, 
		  	IScanSettings _settings) {
			settings = _settings;
			windowResource = _windowResource;
		}
		
		public IList<Image> Images() {
			return images;
		}
		
		public List<Exception> Errors() {
			return errors;
		}
		
		public void finish() {
			if(scanner != null)
				scanner.finish();
		}
		
		private void DeviceLock() {
			padoclient.Log.info("lock region: verify...");
			Console.WriteLine("lock region: verify...");
			string saveDir = Utility.ApplicationPath()+"/Data";
			if(!Directory.Exists(System.IO.Path.GetFullPath(saveDir))) {
				// Create Data folder
        		System.IO.Directory.CreateDirectory(System.IO.Path.GetFullPath(saveDir));
			}
			saveDir = Utility.ApplicationPath()+"/Data/Scanner";
			if(!Directory.Exists(System.IO.Path.GetFullPath(saveDir))) {
				// Create Data folder
        		System.IO.Directory.CreateDirectory(System.IO.Path.GetFullPath(saveDir));
			}
			
			// Combine the new file name with the path
        	string LockFile = System.IO.Path.Combine(System.IO.Path.GetFullPath(saveDir), "scanner.lock");
			if (System.IO.File.Exists(LockFile))
				throw new Exception("Cannot use scanner locked by another HTTP Request");
			
			// create a writer and open the file
            TextWriter tw = new StreamWriter(LockFile);

            // write a line of text to the file
            tw.WriteLine(DateTime.Now);

            // close the stream
            tw.Close();
			
		}
	}
	
	public class TwainScannerThread
	{
		// TimeSpan(h, m, s): Initializes a new TimeSpan to a specified number of hours, 
		// minutes, and seconds.
		
		// Big Wait
		private TimeSpan bigWaitTime = new TimeSpan(1, 0, 0);
		
		// Medium Wait
		private TimeSpan mediumWaitTime = new TimeSpan(0, 30, 0);
		
		// Small Wait
		private TimeSpan smallWaitTime = new TimeSpan(0, 1, 0);
		
		// Fast Wait
		private TimeSpan fastWaitTime = new TimeSpan(0, 0, 25);

		public IList<Image> images = new List<Image>();
		
	    public IList<Image> Scan(MyWinFormsWindowMessageHook windowResource, 
			IScanSettings settings, int timeout)
	    {		
			
			// before to launch scanner find all webserver threads
			//List<int> webthreads = activeThreads();
			
	        // Create the thread object. This does not start the thread.
	        TwainScannerWork workerObject = new TwainScannerWork(windowResource, settings);
	        Thread workerThread = new Thread(workerObject.DoWork);
			workerThread.Name = "TwainScannerThread";
	
	        // Start the worker thread.
	        workerThread.Start();
			padoclient.Log.info("main thread: Starting worker thread...");
	        Console.WriteLine("main thread: Starting worker thread...");
	
	        // Loop until worker thread activates.
	        while (!workerThread.IsAlive);
	
	        // Use the Join method to block the current thread 
	        // until the object's thread terminates.
			TimeSpan waitTimeout = new TimeSpan(0, 0, timeout);
			Console.WriteLine("sync thread: Need to known if working or not ...");
			if(!workerThread.Join(waitTimeout)) {
				workerObject.finish();
				//Console.WriteLine("worker thread: Abort...");
				workerThread.Abort();
			}
			
			/*
			foreach(IntPtr t in webthreads) {
				Console.WriteLine("HTTP thread: {0}", t);
			}
			*/
			
			
			// Un lock device
			DeviceUnLock();
			
			
			// get images
			//Console.WriteLine("main thread: Worker thread has terminated.");
			if(workerObject.Images() != null) {
				foreach(Image twainImage in workerObject.Images()) {
					images.Add(twainImage);
				}
			}
			
			//Console.WriteLine("Scanned images: " + images.Count.ToString());
			//padoclient.Log.info("Scanned images: " + images.Count.ToString());
			
			
			// If installed as window service generate problems
			/*
			 * 
			// after alla images are in memory
			// delete scanner threads
			List<int> allthreads = activeThreads();
			List<int> scanthreads = new List<int>();
			foreach(int t in allthreads) {
				if(!webthreads.Contains(t))
					scanthreads.Add(t);
			}
			

			// GC unused thread
			try {
				foreach(int s in scanthreads) {
					IntPtr ptrThread = OpenThread(1, false, (uint)s);
	                try
	                {
	                    TerminateThread(ptrThread, 1);
	                }
	                catch(Exception e)
	                {
	                    Console.WriteLine(e.ToString());
	                }
				}
			}
			catch(Exception ex) {
				padoclient.Log.fatal(ex.Message);
			}
			*/
			
			if(workerObject.Errors().Count > 0) {
				foreach(Exception ex in workerObject.Errors()) {
					throw ex;
				}
			}
			return images;
	    }
		
		private void DeviceUnLock() {
			string LockFile = Utility.ApplicationPath()+"/Data/Scanner";
			LockFile = System.IO.Path.Combine(System.IO.Path.GetFullPath(LockFile), "scanner.lock");
			System.IO.File.Delete(LockFile);
		}
		
		// If installed as window service generate problems
		/* 
		[DllImport("kernel32.dll")]
        static extern IntPtr OpenThread(uint dwDesiredAccess, bool bInheritHandle, uint dwThreadId);

        [DllImport("kernel32.dll")]
        static extern bool TerminateThread(IntPtr hThread, uint dwExitCode);
		
		// known threads
		public List<int> activeThreads() {
			Process myproc = Process.GetCurrentProcess();
			List<int> threads = new List<int>();
			foreach(Process activeProcess in 
			       	Process.GetProcessesByName(myproc.ProcessName)) {
				foreach (ProcessThread pt in activeProcess.Threads) {
					threads.Add(pt.Id);
				}
			}
			return threads;
		}
		*/
		
		
	}

	
	
}

