
using System;
using System.IO;
using System.Globalization;

namespace padoclient
{
	public class LogSettings
	{
		public string dataDir = "Logs";
		public string logLevel = "debug"; // debug, info, fatal
		public LogSettings()
		{	
		}
	}

	public class Log
	{
		private FileInfo _log;
		private StreamWriter _stream;
		private LogSettings _settings;
		public Log ()
		{
			this.settings = new LogSettings();
			string logfile = "padoclient.log";
			string execpath = Utility.ApplicationPath();
			if(System.IO.Directory.Exists(System.IO.Path.GetFullPath(execpath+"/"+this.settings.dataDir))) {
				logfile = System.IO.Path.GetFullPath(execpath+"/"+this.settings.dataDir+"/"+logfile);
			}
			this.log = new FileInfo(logfile);
			if(!this.log.Exists) {
				this.stream = this.log.CreateText();
				this.stream.Close();
			}
		}
		
	    public LogSettings settings 
	    {
	      get { return _settings; }
	      set { _settings = value; }
	    }		
		
	    public FileInfo log 
	    {
	      get { return _log; }
	      set { _log = value; }
	    }
		
	    public StreamWriter stream 
	    {
	      get { return _stream; }
	      set { _stream = value; }
	    }	
		
	    public void save(string value)
	    {
			try {
				this.stream = File.AppendText(this.log.ToString());
				this.stream.WriteLine(value);
				this.stream.Close();
			}	
			catch(System.IO.IOException ioex) {
				// do nothing probably log file is unaccessible cause user is reading it
				// Console.WriteLine(ioex.ToString());
			}
	    }
		
		public static void debug(string value)
		{
			
			Log oLog = new Log();
			if(oLog.settings.logLevel == "debug") {
				string log = String.Format("{0} - DEBUG: {1}", DateTime.Now.ToString(), value);
				oLog.save(log);
			}
		}		
		
		public static void info(string value)
		{
			Log oLog = new Log();
			if(oLog.settings.logLevel != "fatal") {
				string log = String.Format("{0} - INFO: {1}", DateTime.Now.ToString(), value);
				oLog.save(log);
			}
		}
		
		public static void fatal(string value)
		{
			Log oLog = new Log();
			string log = String.Format("{0} - FATAL: {1}", DateTime.Now.ToString(), value);
			oLog.save(log);
		}
		
	}
}
