using System;
using System.Data.Linq;
using System.Collections.Generic;
using System.Drawing;
using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Drawing;

namespace padoclient
{
	public class Pdf
	{
		private PdfDocument document = new PdfDocument();

		public Pdf ()
		{
			document.Info.Title = "Document";
			document.Info.Author = "Comune di Padova";
			document.Info.Subject = "Scanner digita acquired document";
			document.Info.Keywords = "PDFsharp, XGraphics, Scanner, Padoclient";
		}
		
		// Add single page from image of page format (A4, A3) useful for scanner
		public void AddPage(Image image, PageFormat format) {
			PdfPage page = document.AddPage();
			XGraphics gfx = XGraphics.FromPdfPage(page);
			XImage ximage = XImage.FromGdiPlusImage(image);
			
			// get size of page
			Format pageformat = XSizeConverter.ToPage(image.Size, format);
			
			// get orientation
			if(image.Size.Height > image.Size.Width) {
				page.Orientation = PageOrientation.Portrait;
				page.Size = pageformat.pdfsize;
				gfx.DrawImage(ximage, 0, 0, pageformat.imagesize.Width, pageformat.imagesize.Height);
			}
			else {
				page.Orientation = PageOrientation.Landscape;
				page.Size = pageformat.pdfsize;
				gfx.DrawImage(ximage, 0, 0, pageformat.imagesize.Height, pageformat.imagesize.Width);
			}

		}
		
		public void Save(string filepath) {
			document.Save(filepath);
		}
		
	}
	
	public class Format {
		
		private Size _imagesize;
		private PageSize _pdfsize;
		
		public Format() {
		}
		
		public Size imagesize
		{
	      get { return _imagesize; }
	      set { _imagesize = value; }
	    }
		
		public PageSize pdfsize
		{
	      get { return _pdfsize; }
	      set { _pdfsize = value; }
	    }
		
	}
	
	/// <summary>
	/// Converter from <see cref="XSize"/> to <see cref="PageSize"/>.
	/// </summary>
	public class XSizeConverter
	{
		/// <summary>
		/// Get page area
		/// </summary>
		public static int Area(int w, int h) {
			return w*h;
		}
		
		/// <summary>
		/// Converts the specified page size enumeration to a pair of values in point.
		/// </summary>
		public static Format ToPage(XSize value, PageFormat format)
		{
			// Default page size
			Format page = new Format();
			if(format == null) {
				page.imagesize =new Size(595, 842);
				page.pdfsize = PageSize.A4;
			}
			
			// choose proportions
			/*
			Dictionary<PageSize, Size> formats = new Dictionary<PageSize, Size>();
			formats.Add(PageSize.A0, new Size(2380, 3368));
			formats.Add(PageSize.A1, new Size(1684, 2380));
			formats.Add(PageSize.A2, new Size(1190, 1684));
			formats.Add(PageSize.A3, new Size(842, 1190));
			formats.Add(PageSize.A4, new Size(595, 842));
			formats.Add(PageSize.A5, new Size(420, 595));
			formats.Add(PageSize.RA0, new Size(2438, 3458));
			formats.Add(PageSize.RA1, new Size(1729, 2438));
			formats.Add(PageSize.RA2, new Size(1219, 1729));
			formats.Add(PageSize.RA3, new Size(865, 1219));
			formats.Add(PageSize.RA4, new Size(609, 865));
			formats.Add(PageSize.RA5, new Size(343, 609));
			formats.Add(PageSize.B0, new Size(2835, 4008));
			formats.Add(PageSize.B1, new Size(2004, 2835));
			formats.Add(PageSize.B2, new Size(4252, 1417));
			formats.Add(PageSize.B3, new Size(1001, 1417));
			formats.Add(PageSize.B4, new Size(729, 1032));
			formats.Add(PageSize.B5, new Size(516, 729));
			formats.Add(PageSize.Quarto, new Size(576, 720));
			formats.Add(PageSize.Foolscap, new Size(576, 936));
			formats.Add(PageSize.Executive, new Size(540, 720));
			formats.Add(PageSize.GovernmentLetter, new Size(756, 576));
			formats.Add(PageSize.Letter, new Size(612, 792));
			formats.Add(PageSize.Legal, new Size(612, 1008));
			formats.Add(PageSize.Ledger, new Size(1224, 792));
			//formats.Add(PageSize.Tabloid, new Size(792, 1224)); # already exists area
			formats.Add(PageSize.Post, new Size(1126, 1386));
			formats.Add(PageSize.Crown, new Size(1440, 1080));
			formats.Add(PageSize.LargePost, new Size(1188, 1512));
			formats.Add(PageSize.Demy, new Size(1260, 1584));
			formats.Add(PageSize.Medium, new Size(1296, 1656));
			formats.Add(PageSize.Royal, new Size(1440, 1800));
			formats.Add(PageSize.Elephant, new Size(1565, 2016));
			formats.Add(PageSize.DoubleDemy, new Size(1692, 2520));
			formats.Add(PageSize.QuadDemy, new Size(2520, 3240));
			//formats.Add(PageSize.STMT, new Size(396, 612)); # already exists area
			formats.Add(PageSize.Folio, new Size(612, 936));
			formats.Add(PageSize.Statement, new Size(396, 612));
			formats.Add(PageSize.Size10x14, new Size(720, 1008));
			*/

			switch(format) {
				case PageFormat.A4:
					page.imagesize = new Size(595, 842);
					page.pdfsize = PageSize.A4;
					break;
				case PageFormat.A3:
					page.imagesize = new Size(842, 1190);
					page.pdfsize = PageSize.A3;
					break;
				default:
					page.imagesize = new Size(595, 842);
					page.pdfsize = PageSize.A4;
					break;
			}
			return page;
		}
	}
	
}

