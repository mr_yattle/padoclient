using System;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.BZip2;
using ICSharpCode.SharpZipLib.Tar;
using System.Collections.Generic;
using System.IO;

namespace padoclient
{
	public class Compress
	{
		
		/// <summary>
		/// True if we are to convert ASCII text files from local line endings
		/// to the UNIX standard '\n'.
		/// </summary>
		bool asciiTranslate;
		
		/// <summary>
		/// True if we are not to overwrite existing files.  (Unix noKlobber option)
		/// </summary>
		bool keepOldFiles;
		
		/// <summary>
		/// The blocking factor to use for the tar archive IO. Set by the '-b' option.
		/// </summary>
		int blockingFactor;
		
		/// <summary>
		/// The userId to use for files written to archives. Set by '-U' option.
		/// </summary>
		int userId;
		
		/// <summary>
		/// The userName to use for files written to archives. Set by '-u' option.
		/// </summary>
		string userName;
		
		/// <summary>
		/// The groupId to use for files written to archives. Set by '-G' option.
		/// </summary>
		int groupId;
		
		/// <summary>
		/// The groupName to use for files written to archives. Set by '-g' option.
		/// </summary>
		string groupName;
		
		private TarArchive tar = null;
		private List<string> filenames = null;
		
		public Compress ()
		{
			filenames = new List<string>();
			
			// Tar params
			blockingFactor = TarBuffer.DefaultBlockFactor;
			userId   = 0;
			string sysUserName = Environment.UserName;
			userName = ((sysUserName == null) ? "" : sysUserName);
			groupId   = 0;
			groupName = "None";
		}
		
		public void AddFile(string filepath) {
			filenames.Add(filepath);
		}
		
		public void Gzip(string destination) {
			using (GZipOutputStream outputstream = new GZipOutputStream(File.Create(destination))) {
				outputstream.SetLevel(9); // 0 - store only to 9 - means best compression
				tar = TarArchive.CreateOutputTarArchive(outputstream, blockingFactor);
				
				// options
				tar.SetKeepOldFiles(keepOldFiles);
				tar.AsciiTranslate = asciiTranslate;
				tar.SetUserInfo(userId, userName, groupId, groupName);
				
				foreach (string filepath in filenames) {
					TarEntry entry = TarEntry.CreateEntryFromFile(filepath);
					entry.Name = Path.GetFileName(filepath);
					tar.WriteEntry(entry, true);
				}
				
				// close tar
				tar.Close();
			}
		}
	}
}

