$(document).ready(function() {

	$.ajax({
		type: "GET",
		url: "retrieve.json",
		dataType: "json",
		success: function(data){
			$("#port").val(data.port);
			// reset ipaddresses
			$("#ipaddress option").remove();
			$.each(data.ipaddresses, function(index, address) {
				if(address.selected == 1) {
					$('#ipaddress').
						append($("<option></option>").
						attr("value",address.ip).
						attr("selected",true).
						text(address.value)); 
				}
				else {
					$('#ipaddress').
						append($("<option></option>").
						attr("value",address.ip).
						text(address.value)); 
				}
			});
			$("#username").val(data.username);
		},
		error:function (xhr, ajaxOptions, thrownError){
			alert(xhr.status+" "+xhr.statusText);
		} 
	});
	
    // bind to the form's submit event 
    $('#formConfigure').submit(function() { 
		// Before submit check: username, password
		if($("#username").val() == '') {
			alert("Must specify username");
			return false;
		}
		
		// Check password validation
		var passed = validateUsername($("#username").val(), {
			length:   [4, Infinity],
			lower:    0,
			upper:    0,
			numeric:  0,
			special:  0,
			badWords: []
		});
		if(!passed) {
			alert("Username must me 4-20 chars, no special chars");
			return false;
		}
		
		if($("#password").val() == '') {
			alert("Must specify password");
			return false;
		}
		
		if($("#password-re").val() == '') {
			alert("Must specify Re password to ensure is correct");
			return false;
		}
		
		if($("#password").val() != $("#password-re").val()) {
			alert("Passwords does not match");
			return false;
		}
	
		// Check password validation
		var passed = validatePassword($("#password").val(), {
			length:   [4, Infinity],
			lower:    0,
			upper:    0,
			numeric:  1,
			special:  0,
			badWords: []
		});
		if(!passed) {
			alert("Password must me 4-20 chars, must contains number, no special chars");
			return false;
		}
		
	
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit({
			type: 'GET',
			dataType: 'json',
			success: processJson,
			error: ajaxError}); 
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false; 
    }); 
	
});


/*
	Emulates password validator
*/
function validateUsername (pw, options) {
	return validatePassword(pw, options);
}

/*
	Password Validator 0.1
	(c) 2007 Steven Levithan <stevenlevithan.com>
	MIT License
*/

function validatePassword (pw, options) {
	// default options (allows any password)
	var o = {
		lower:    0,
		upper:    0,
		alpha:    0, /* lower + upper */
		numeric:  0,
		special:  0,
		length:   [0, Infinity],
		custom:   [ /* regexes and/or functions */ ],
		badWords: [],
		badSequenceLength: 0,
		noQwertySequences: false,
		noSequential:      false
	};

	for (var property in options)
		o[property] = options[property];

	var	re = {
			lower:   /[a-z]/g,
			upper:   /[A-Z]/g,
			alpha:   /[A-Z]/gi,
			numeric: /[0-9]/g,
			special: /[\W_]/g
		},
		rule, i;

	// enforce min/max length
	if (pw.length < o.length[0] || pw.length > o.length[1])
		return false;

	// enforce lower/upper/alpha/numeric/special rules
	for (rule in re) {
		if ((pw.match(re[rule]) || []).length < o[rule])
			return false;
	}

	// enforce word ban (case insensitive)
	for (i = 0; i < o.badWords.length; i++) {
		if (pw.toLowerCase().indexOf(o.badWords[i].toLowerCase()) > -1)
			return false;
	}

	// enforce the no sequential, identical characters rule
	if (o.noSequential && /([\S\s])\1/.test(pw))
		return false;

	// enforce alphanumeric/qwerty sequence ban rules
	if (o.badSequenceLength) {
		var	lower   = "abcdefghijklmnopqrstuvwxyz",
			upper   = lower.toUpperCase(),
			numbers = "0123456789",
			qwerty  = "qwertyuiopasdfghjklzxcvbnm",
			start   = o.badSequenceLength - 1,
			seq     = "_" + pw.slice(0, start);
		for (i = start; i < pw.length; i++) {
			seq = seq.slice(1) + pw.charAt(i);
			if (
				lower.indexOf(seq)   > -1 ||
				upper.indexOf(seq)   > -1 ||
				numbers.indexOf(seq) > -1 ||
				(o.noQwertySequences && qwerty.indexOf(seq) > -1)
			) {
				return false;
			}
		}
	}

	// enforce custom regex/function rules
	for (i = 0; i < o.custom.length; i++) {
		rule = o.custom[i];
		if (rule instanceof RegExp) {
			if (!rule.test(pw))
				return false;
		} else if (rule instanceof Function) {
			if (!rule(pw))
				return false;
		}
	}

	// great success!
	return true;
}


