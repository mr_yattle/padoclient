// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
    // for normal html responses, the first argument to the success callback 
    // is the XMLHttpRequest object's responseText property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'xml' then the first argument to the success callback 
    // is the XMLHttpRequest object's responseXML property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'json' then the first argument to the success callback 
    // is the json data object returned by the server 
 
    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
        '\n\nThe output div should have already been updated with the responseText.'); 
} 

function ajaxError(xhr, ajaxOptions, thrownError){
	stopLoading();
	alert(xhr.status+" "+xhr.statusText);
	if(confirm('Show full error ?')) {
		alert(thrownError);
	}
} 

function startLoading() {
	$("#loading").show();
}

function stopLoading() {
	$("#loading").hide();
}

function processJson(data) { 
    // 'data' is the json object returned from the server 
	try {
		if(data.status == 1) { // NORMAL
			// multiple message
			if(data.messages.length > 0) {
				var messages = data.messages.join("\n");
				alert(messages);
			}
		}
		if(data.status == 2) { // ERRORS
			// multiple message
			if(data.errors.length > 0) {
				var errors = data.errors.join("\n");
				alert(errors);
			}
		}
		if(data.status == 0) { // SERVER NOT HANDLED
			// multiple message
			alert("Missing command or unimplemented");
		}
	}
	catch(e) {
		alert(e);
	}
}