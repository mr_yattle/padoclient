function loadInterface() {
	// Reaload all info about selected scanner
	$.ajax({
		type: "GET",
		url: "retrieve.json",
		dataType: "json",
		success: function(data){
			// avaible models
			// reset all model options
			$("#model option").remove();
			$.each(data.models, function(key, model)
			{   
				if(model.selected == 1) {
					$('#model').
						append($("<option></option>").
						attr("value",model.name).
						attr("selected",true).
						text(model.name)); 
				}
				else {
					$('#model').
						append($("<option></option>").
						attr("value",model.name).
						text(model.name)); 
				}
			});
			
			// avaible dpi
			// reset all dpi options
			$("#dpi option").remove();
			$.each(data.dpi, function(key, value)
			{   
				$('#dpi').
					append($("<option></option>").
					attr("value",value).
					text(value)); 
			});
			
			
			// avaible format
			// reset all page-format options
			$("#page-format option").remove();
			$.each(data.formats, function(key, value)
			{   
				$('#page-format').
					append($("<option></option>").
					attr("value",value).
					text(value)); 
			});
			
			// avaible orientation
			// reset all page-orientation options
			$("#page-orientation option").remove();
			var orientations = {L:"Landscape", P:"Portrait"};
			$.each(data.orientations, function(key, value)
			{   
				$('#page-orientation').
					append($("<option></option>").
					attr("value",value).
					text(orientations[value])); 
			});
			
			// Load preview splash
			loadPreview();
			
		},
		error:ajaxError
	});
		
}

function loadPreview() {
	// Load all info about preview
	startLoading();
	$.ajax({
		type: "GET",
		url: "scaninfo.json",
		dataType: "json",
		success: function(data){
			// reset all thumbs options
			$(".thumbox li").remove();
			var selected = " class=\"selected\" ";
			$.each(data.jpgs, function(key, jpg) {
				if(key == 0)
					loadPreviewImage("../Data/Scanner/"+jpg+"?height=280&width=280");
				if(key != 0)
					selected = "";
				$(".thumbox").append(
					"<li class=\"thumb\">"
					+ " <a "+selected+" href=\"../Data/Scanner/"+jpg+"\">"
					+ "  <img title=\"Thumb "+(key+1)+"\" src=\"../Data/Scanner/"+jpg+"?height=80&width=80\">"
					+ " </a>"
					+ " <i>Page "+(key+1)+"</i>"
					+ "</li>");
			});
			
			// clean download section
			$(".formats li").remove(); 
			if(data.pdf.filename != null && data.pdf.filename != "") {
				$(".formats").append(
					"<li class=\"pdf\">"
					+ " <a href=\"../Data/Scanner/"+data.pdf.filename+"?download=1\">"
					+ "  <b>Download</b>"
					+ " </a>"
					+ " <i>Save scanned document in Pdf/Multipaged format</i>"
					+ "</li>");
			}
	
			// Handle select thumb action
			$(".thumbox a").click(function() {
				loadPreviewImage($(this).attr("href")+"?height=300&width=300");
				$(".thumbox").find("a").each(function(i){
					$(this).removeClass("selected");
				});
				// select
				$(this).toggleClass("selected");
				return false;
			});
			
			// load all
			stopLoading();
		},
		error:ajaxError
	});
}

function loadPreviewImage(image) {
	// Load image
	if($("#page-orientation").val() == 'L') {
		$("#preview-orientation").removeClass("portrait");
		if(!$("#preview-orientation").hasClass("landscape"))
			$("#preview-orientation").toggleClass("landscape");
	}
	else {
		$("#preview-orientation").removeClass("landscape");
		if(!$("#preview-orientation").hasClass("portrait"))
			$("#preview-orientation").toggleClass("portrait");
	}
	$("#page-preview img").attr("src", image);
}

$(document).ready(function() {

	loadInterface(); // load all interface widgets data

    // bind to the form's submit event 
    $('#model').change(function() { 
		alert("ATTENTION! Select scanner window will appear beside this window.");
		startLoading();
		$.ajax({
			type: "GET",
			url: "change.do",
			dataType: "json",
			success: function(data) {
				if(data.status == 1) {
					// reload scan interface
					loadInterface();
				}
				processJson(data);
				stopLoading();
			},
			error: ajaxError
		});
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false; 
    }); 
	
    // bind to the form's submit event 
    $('#formScanner').submit(function() { 	
		lockScan();
		startLoading();
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit({
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				unlockScan(data);
				processJson(data);
				if(data.status == 1) {
					// Load scanned images
					loadPreview();
				}
				stopLoading();
			},
			error: ajaxError}); 
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false; 
    }); 
	
});

function unlockScan(data) {
	$("#scan-start").attr("disabled", false);
}

function lockScan() {
	// Cannot click button until SCAN ends
	$("#scan-start").attr("disabled", true);
}

// pre-submit callback 
function showRequest(formData, jqForm, options) { 
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData); 
 
    // jqForm is a jQuery object encapsulating the form element.  To access the 
    // DOM element for the form do this: 
    // var formElement = jqForm[0]; 
 
    alert('About to submit: \n\n' + queryString); 
 
    // here we could return false to prevent the form from being submitted; 
    // returning anything other than false will allow the form submit to continue 
    return false; 
} 