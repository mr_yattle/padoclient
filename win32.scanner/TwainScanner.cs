using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TwainDotNet;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using TwainDotNet.WinFroms;
using TwainDotNet.TwainNative;
using TwainDotNet.Win32;
using System.Diagnostics;

namespace win32scanner
{
	
	public enum TwainCommand
	{
		Not				= -1,
		Null			= 0,
		TransferReady	= 1,
		CloseRequest	= 2,
		CloseOk			= 3,
		DeviceEvent		= 4
	}
	
	public class IPageSettings: PageSettings
	{
	}
	
	public class IScanSettings: ScanSettings
	{
		private Dictionary<string, PageType> _paperFormat = new Dictionary<string, PageType>();
		private Dictionary<string, int> _avaibleDpi = new Dictionary<string, int>();
		private Dictionary<string, TwainDotNet.TwainNative.Orientation> _orientation = new Dictionary<string, TwainDotNet.TwainNative.Orientation>();
		
		public IScanSettings(): base() {
			// Page formats
			_paperFormat.Add("A4", PageType.A4);
			_paperFormat.Add("A3", PageType.A3);
			
			// Dpi
			_avaibleDpi.Add("75", 75);
			_avaibleDpi.Add("100", 100);
			_avaibleDpi.Add("150", 150);
			_avaibleDpi.Add("300", 300);
			_avaibleDpi.Add("600", 600);
			
			// Orientation
			_orientation.Add("P", TwainDotNet.TwainNative.Orientation.Portrait);
			_orientation.Add("L", TwainDotNet.TwainNative.Orientation.Landscape);
		}
		
		public Dictionary<string, PageType> formats() {
			return _paperFormat;
		}
		
		public Dictionary<string, TwainDotNet.TwainNative.Orientation> orientations() {
			return _orientation;
		}
		
		public Dictionary<string, int> dpis() {
			return _avaibleDpi;
		}
		
		public ResolutionSettings res() {
			if(Resolution != null) {
				return Resolution;
			}
            else {
				initAttributes();
				return Resolution;
            }
		}
		
		public PageSettings pag() {
			if(Page != null) {
				return Page;
			}
            else {
				initAttributes();
				return Page;
            }
		}
		
		public void initAttributes() {
			Resolution = ScanSettings.Default.Resolution;
			Page = ScanSettings.Default.Page;
		}
		
		public void dpi(string n) {
			try {
				this.res().Dpi = _avaibleDpi[n];
			}
			catch(KeyNotFoundException) {
				Log.fatal("Dpi not supported: "+n);
			}
		}
		
		public void grayscale() {
			this.res().ColourSetting = TwainDotNet.ColourSetting.GreyScale;
		}
		
		public void blackandwhite() {
			this.res().ColourSetting = TwainDotNet.ColourSetting.BlackAndWhite;
		}
		
		public void color() {
			this.res().ColourSetting = TwainDotNet.ColourSetting.Colour;
		}
		
		public void paperFormat(string format) {
			try {
				this.pag().Size = _paperFormat[format];
			}
			catch(KeyNotFoundException) {
				Log.fatal("Paper format not supported: " + format);
			}
		}
		
		public void orientation(string orientation) {
			try {
				this.pag().Orientation = _orientation[orientation];
			}
			catch(KeyNotFoundException) {
				Log.fatal("Orientation ["+orientation+"] not classified");
			}
		}
		
	}
	
	public class CapabilityItem
	{
		private string _name;
		private bool _supported;
		public CapabilityItem(string n, bool s)
		{
			_name = n;
			_supported = s;
		}
		public string name { 
			get { return this._name; }
		}
		public bool supported { 
			get { return this._supported; }
		}
	}
	
	public class ScannerInfo
	{
		public string Manufactor;
		public string ProductName;
		public string ProductFamily;
		public ScannerInfo(Identity twainIdentity)
		{
			Manufactor = twainIdentity.Manufacturer;
			ProductName = twainIdentity.ProductName;
			ProductFamily = twainIdentity.ProductFamily;
		}
	}
	
    public class MyDiagnostics
    {
		private IWindowsMessageHook _messageHook;
		DataSourceManager _dataSourceManager;
		
        public MyDiagnostics(IWindowsMessageHook messageHook)
        {
				_messageHook = messageHook;
				_dataSourceManager = new DataSourceManager(DataSourceManager.DefaultApplicationId, _messageHook);
        }
		
        /// <summary>
        /// Gets a list of sources info (TWAIN device, TWAIN datasource).
        /// </summary>
        public IList<ScannerInfo> sources()
        {
            var result = new List<ScannerInfo>();
            var sources = DataSource.GetAllSources(
                _dataSourceManager.ApplicationId,
                _dataSourceManager.MessageHook);

            foreach (var source in sources)
            {
				ScannerInfo info = new ScannerInfo(source.SourceId);
                result.Add(info);
                source.Dispose();
            }

            return result;
        }
		
        /// <summary>
        /// Gets TWAIN info on scanner source
        /// </summary>
        public ScannerInfo info()
        {
            var source = DataSource.GetDefault(
			    _dataSourceManager.ApplicationId,
                _dataSourceManager.MessageHook);

			ScannerInfo info = new ScannerInfo(source.SourceId);
            source.Dispose();
            return info;
        }
		
        /// <summary>
        /// Get TWAIN Identity from product name
        /// </summary>
        public Identity identity(string sourceName)
        {
			foreach(ScannerInfo info in this.sources()) {
				if(info.ProductName.IndexOf(sourceName) != -1) {
		            var source = DataSource.GetSource(
		                info.ProductName,
		                _dataSourceManager.ApplicationId,
		                _dataSourceManager.MessageHook);
		            return source.SourceId;
				}
			}
			return null;
        }
		
		public List<CapabilityItem> capabilities() 
		{
			try {
	            var dataSource = _dataSourceManager.DataSource;
	            dataSource.OpenSource();
				
				List<CapabilityItem> _capabilities = new List<CapabilityItem>(); 
			    foreach (Capabilities capability in Enum.GetValues(typeof(Capabilities)))
	            {
	                try
	                {
	                    var result = Capability.GetBoolCapability(capability, _dataSourceManager.ApplicationId, dataSource.SourceId);
	
						_capabilities.Add(new CapabilityItem(capability.ToString(), true));
	                    //Console.WriteLine("{0}: {1}", capability, result);
	                }
	                catch (TwainException)
	                {
						_capabilities.Add(new CapabilityItem(capability.ToString(), false));
	                    //Console.WriteLine("{0}: {1} {2} {3}", capability, e.Message, e.ReturnCode, e.ConditionCode);
	                }
	            }
				return _capabilities;
			}
			catch(TwainException) {
				return new List<CapabilityItem>();
			}
		} 
    }
	
	
	public class Tray : Form
	{
		
      public NotifyIcon  trayIcon;
      private ContextMenu trayMenu;
		
      public void initializeContext()
      {			
          // Create a simple tray menu with only one item.
          trayMenu = new ContextMenu();
          //trayMenu.MenuItems.Add("Exit", OnExit);

          // Create a tray icon. In this example we use a
          // standard system icon for simplicity, but you
          // can of course use your own custom icon too.
          trayIcon      = new NotifyIcon();
          trayIcon.Text = "Web Scanner device";
		  //string execpath = Environment.CurrentDirectory; // Command Line
			string execpath = System.AppDomain.CurrentDomain.BaseDirectory; // System Service
          trayIcon.Icon = new Icon(execpath + "\\Images\\scanner.ico");

          // Add menu to tray icon and show it.
          trayIcon.ContextMenu = trayMenu;
          //trayIcon.Visible     = true;
		  trayIcon.Visible     = false;
      }
		
	  public virtual void CloseTray() {
		 Application.Exit();
	  } 
	

      private void OnExit(object sender, EventArgs e)
      {
		  CloseTray();
      }
		
	  protected override void OnLoad(EventArgs e)
      {
          Visible       = false; // Hide form window.
          ShowInTaskbar = false; // Remove from taskbar.

          base.OnLoad(e);
      }
		
      protected override void Dispose(bool isDisposing)
      {
          if (isDisposing)
          {
              // Release the icon resource.
              trayIcon.Dispose();
			  GC.SuppressFinalize(this); 
          }

          base.Dispose(isDisposing);
      }
	}
	
	public class MyWinFormsWindowMessageHook : WinFormsWindowMessageHook
	{	
		public MyWinFormsWindowMessageHook(Form window)
			: base(window) {
        }
		
        public override bool PreFilterMessage(ref System.Windows.Forms.Message m)
        {
            if (FilterMessageCallback != null)
            {
                bool handled = true;
				try {
                	FilterMessageCallback(m.HWnd, m.Msg, m.WParam, m.LParam, ref handled);
				}
				catch(CloseWindowException) {
					Application.Exit();
				}
				catch(Exception  ex) {
					Console.WriteLine (ex.ToString());
				}
                return handled;
            }
			return false;
        }
		
	}
	
	public class TwainScanner: Tray
	{
		
		// IWindowsMessageHook
		private Twain _hwScanner;
		private MyWinFormsWindowMessageHook _windowResource;
		
		public TwainScanner ()
		{
			_TwainScanner();
		}
		
		public TwainScanner (bool safe_mode)
		{
			try {
				_TwainScanner();
			}
			catch(TwainException) {
				// Log.fatal(ex.ToString());
			}
		}
		
		public virtual void _TwainScanner ()
		{
			_windowResource = new MyWinFormsWindowMessageHook(new Form());
			_hwScanner = new Twain(_windowResource);
		}
		
		public void systray() {
			this.initializeContext();
		}
		
		public override void CloseTray() {
			this.finish();
		}
		
		public void finish()
		{
			Log.info("Stop Scanning .....");
			try {
				this.trayIcon.Dispose();
				this._hwScanner.Dispose();
			}
			catch(NullReferenceException) {
				// Missing trayIcon
			}
			Application.Exit();
		}
		
        ~TwainScanner()
        {
            this.finish();
        }
		
		public IList<string> list() {
			try {
				return _hwScanner.SourceNames;
			}
			catch (NullReferenceException) {
				Log.fatal("No scanner detected !");
				return null;
			}
		}
		
		public bool chooseBeforeScan(string scannerName) {
			try {
				_hwScanner.SelectSource(scannerName);
				return true;
			}
			catch (NullReferenceException) {
				Log.fatal("No scanner detected !");
				this.finish();
				return false;
			}
			
		}
				
		public bool choose() {
			try {
				DataSourceManager _dataSourceManager = new DataSourceManager(
					DataSourceManager.DefaultApplicationId, 
				    _windowResource);
				_dataSourceManager.SelectSource();
				return true;
			}
			catch (TwainException) {
				Log.fatal("No scanner detected !");
				this.finish();
				return false;
			}
		}
		
		public string scannerName() {
			try {
				return _hwScanner.DefaultSourceName;
			}
			catch (NullReferenceException) {
				return null;
			}
		}
		
		public bool read(ScanSettings settings) {
			Log.info("Scanning in progress ....");
			//TwainLock(false);	
			_hwScanner.StartScanning(settings);
			// Handle settings errors message from scanner
			foreach(Exception ex in _hwScanner.SettingsExceptions) {
				Log.info(ex.Message);
			}
			_hwScanner.ScanningComplete += delegate {
				this.save();
			};
			return true;
		}
		
		public bool acquire(ScanSettings settings, bool ignoreErrors) {
			this.systray();
			if(ignoreErrors) {
				try {
					return this.read(settings); 
				}
				catch (NullReferenceException ex) {
					Log.fatal("Cannot find TWAIN datasource, verify scanner connection: " + ex.ToString());
					this.finish();
				}
				catch (Exception ex) {
					Log.fatal(ex.ToString());
					this.finish();
				}
			}
			else {
				return this.read(settings);
			}
			return false;
		}
		
		public bool acquire(ScanSettings settings) {
			return acquire(settings, true);
		}
		
		public bool _scan() {
			ScanSettings settings = new ScanSettings();
			return this.acquire(settings);
		}
		
		private void save() {
			/*
            if (_hwScanner.Images.Count > 0)
            {
                Image scanImage = _hwScanner.Images[0];
                _hwScanner.Images.Clear();
				Console.WriteLine(scanImage);
            }
            */
			this.finish();
		}
		
        /// <summary>
        /// The scanned in images.
        /// </summary>
        public IList<Image> Images { 
			get {
				try {
					return _hwScanner.Images;
				}
				catch(NullReferenceException)
				{
					return new List<Image>();
				}
			} 
		}
		
	    [STAThread]
	    public void scan(IScanSettings settings)
	    {
			if(this.acquire(settings))
				Application.Run(this);
	    }
		
	    [STAThread]
	    public void scan(IScanSettings settings, bool ignoreErrors)
	    {
			if(this.acquire(settings, ignoreErrors))
				Application.Run(this);
	    }
		
	    public MyDiagnostics diagnostic()
	    {
			return new MyDiagnostics(_windowResource);
	    }

	    public List<CapabilityItem> capabilities()
	    {
			MyDiagnostics test = new MyDiagnostics(_windowResource);
			return test.capabilities();
	    }
		
		// Resolution in monitor
		[DllImport("gdi32.dll", ExactSpelling=true)]
		private static extern int GetDeviceCaps( IntPtr hDC, int nIndex );
	
		[DllImport("gdi32.dll", CharSet=CharSet.Auto)]
		private static extern IntPtr CreateDC( string szdriver, string szdevice, string szoutput, IntPtr devmode );
	
		[DllImport("gdi32.dll", ExactSpelling=true)]
		private static extern bool DeleteDC( IntPtr hdc );
		
		public static int ScreenBitDepth {
			get {
				IntPtr screenDC = CreateDC( "DISPLAY", null, null, IntPtr.Zero );
				int bitDepth = GetDeviceCaps( screenDC, 12 );
				bitDepth *= GetDeviceCaps( screenDC, 14 );
				DeleteDC( screenDC );
				return bitDepth;
			}
		}
	
	}
	
	class Win32
	{
		
		[StructLayout(LayoutKind.Sequential, Pack=4)]
		internal struct WINMSG
		{
			public IntPtr		hwnd;
			public int			message;
			public IntPtr		wParam;
			public IntPtr		lParam;
			public int			time;
			public int			x;
			public int			y;
		}
		
	}
	
	
}

