using System;
using System.Collections.Generic;

namespace Devices
{
	
	interface IDevice
	{
		void gui();
		void listcommands();
		string options { get; set; }
		void execute(string options);
	}
	
	public class Command
	{
		private string _command;
		private string _args;
		
		public Command (string command, string args)
		{
			this._command = command;
			this._args = args;
		}
		
		public Command (string command)
		{
			this._command = command;
			this._args = null;
		}
		
		public string method() {
			return this._command;
		}
		
		public string argvalue() {
			return this._args;
		}
		
		/*
		public Dictionary arguments() {
			string[] parameters = this._args.Split(',');
			Dictionary <String, String> args = new Dictionary <String, String>();
			if(parameters.Length > 0) {
				for (int i = 0; i < parameters.Length; i++) {
					string[] arg = parameters[1].Split('=');
					args.Add(arg[0], arg[1]);
				}
			}
			else if(parameters.Length == 0) {
				args.Add("value", this._args);
			}
			if(args.Count > 0) {
				return args;
			}
			return null;
		}
		*/
		
	}
	
	public class Scanner: IDevice
	{
				
		private string _options;
		private bool _hasgui;
		public TwainScanner tw;
		
		public Scanner ()
		{
			tw = new TwainScanner();
		}
		
		public void listcommands() {
			Console.WriteLine("Test twain device scanner:");
			/// Usage
			Console.WriteLine("");
			Console.WriteLine("Usage: windevices.exe --device=scanner --options=SELECT=scanner01;ADF;COLOR=blue");
			Console.WriteLine("");
			Console.WriteLine(" SELECT     Seleziona lo scanner dal quale eseguire la scansione");
			Console.WriteLine(" LIST       Elenco degli scanner dai quali è possibile eseguire la scansione");
			Console.WriteLine(" SCAN       Esegue la scansione");
		}
		
	  	public bool hasgui
	    {
		    get { return _hasgui; }
		    set { _hasgui = value; }
	    }
		
	  	public string options
	    {
		    get { return _options; }
		    set { _options = value; }
	    }
		
		public Command translateToCommands(string arguments) {
			string[] parts = arguments.Split('=');
			if(parts.Length == 1) {
				Command command = new Command(parts[0]);
				return command;
			}
			if(parts.Length == 2) {
				Command command = new Command(parts[0], parts[1]);
				return command;
			}
			return null;
		}
		
		public List<Command> parseCommands(string options) {
			List<Command> commands = new List<Command>();
			string[] parts = options.Split(';');
			if(parts.Length == 0) {
				Command cmd = this.translateToCommands(options);
				if(!cmd.Equals(null)) {
					commands.Add(cmd);
				}
			}
			else if(parts.Length > 0) {
				for (int i = 0; i < parts.Length; i++) {
					Command cmd = this.translateToCommands(parts[i]);
					if(!cmd.Equals(null)) {
						commands.Add(cmd);
					}
				}
			}
			if(commands.Count > 0) {
				return commands;
			}
			return null;
		}
		
		public void execute(string options)
		{
			
			// Parsing options
			List<Command> commands = this.parseCommands(options);
			foreach (Command cmd in commands) {
				this.executeCommand(cmd);
			}
			
		}
		
		public void executeCommand(Command cmd) {
			Console.WriteLine(cmd.method() + ":" + cmd.argvalue());
			switch( cmd.method() ) {
				case "LIST": this.listScanners(); break;
				case "SELECT": this.selectScanner(cmd.argvalue()); break;
				case "SCAN": this.scan(); break;
				default: Console.WriteLine("Missing command: "+cmd.method()+"!"); return;
			}
		}
		
		public IList<string> listScanners() {
			IList<string> scanners = this.tw.listScanners();
			foreach(string scanner in scanners) {
				Console.WriteLine(scanner);
			}
			return scanners;
		}
		
		public void selectScanner(string scannerName) {
			// FUJITSU fi-5750Cdj
			// WIA-fi-5750Cdj
			this.tw.selectSource(scannerName);
		}
		
		public void scan() {
			this.tw.executeScan();
		}
		
	}
}

