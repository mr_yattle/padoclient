using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TwainDotNet;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using TwainDotNet.WinFroms;

namespace Devices
{
	
	public enum TwainCommand
	{
		Not				= -1,
		Null			= 0,
		TransferReady	= 1,
		CloseRequest	= 2,
		CloseOk			= 3,
		DeviceEvent		= 4
	}
	
	
	public class Tray : Form
	{
		
      public NotifyIcon  trayIcon;
      private ContextMenu trayMenu;
		
      public void initializeContext()
      {			
          // Create a simple tray menu with only one item.
          trayMenu = new ContextMenu();
          trayMenu.MenuItems.Add("Exit", OnExit);

          // Create a tray icon. In this example we use a
          // standard system icon for simplicity, but you
          // can of course use your own custom icon too.
          trayIcon      = new NotifyIcon();
          trayIcon.Text = "Scanner TrayApp";
		  string execpath = Environment.CurrentDirectory;
          trayIcon.Icon = new Icon(execpath + "\\Images\\scanner.ico");

          // Add menu to tray icon and show it.
          trayIcon.ContextMenu = trayMenu;
          trayIcon.Visible     = true;
      }

      private void OnExit(object sender, EventArgs e)
      {
          Application.Exit();
      }
			
		  protected override void OnLoad(EventArgs e)
      {
          Visible       = false; // Hide form window.
          ShowInTaskbar = false; // Remove from taskbar.

          base.OnLoad(e);
      }
		
      protected override void Dispose(bool isDisposing)
      {
          if (isDisposing)
          {
              // Release the icon resource.
              trayIcon.Dispose();
          }

          base.Dispose(isDisposing);
      }
	}
	
	public class TwainScanner: Tray
	{
		
		// IWindowsMessageHook
		private Twain _hwScanner;
		private Form _windowResource;
		
		// Gui
		public TwainScanner ()
		{
			_windowResource = new Form();
			_hwScanner = new Twain(new WinFormsWindowMessageHook(_windowResource));
			initializeContext();
		}
		
		// Gui
		public void initialize ()
		{
			this.scan();
		}
		
		public void Finish()
		{
			Console.WriteLine("Finish Scanning");
		}
		
		public void Acquire()
		{
			// Application.AddMessageFilter( this );
			// Console.WriteLine("Acquire Scanning");
			this.scan();
		}
		
		public void Select()
		{
			_hwScanner.SelectSource();
		}
		
		private void EndingScan()
		{
			Console.WriteLine("End scan");
			// tw.CloseSrc();
		}
		
		public IList<string> listScanners() {
			return _hwScanner.SourceNames;
		}
		
		public void selectSource(string ScannerName) {
			_hwScanner.SelectSource(ScannerName);
		}
		
		public void scan() {
			try {
				Console.WriteLine("Scanning in progress ....");
				ScanSettings _settings = new ScanSettings();
				_settings.UseDocumentFeeder = true;
				_hwScanner.StartScanning(_settings);
				_hwScanner.ScanningComplete += delegate {
					this.save();
				};
			}
			catch (Exception ex) {
				Console.WriteLine(ex.ToString());
			}
		}
		
		public void save() {
                if (_hwScanner.Images.Count > 0)
                {
                    Image scanImage = _hwScanner.Images[0];
                    _hwScanner.Images.Clear();
					Console.WriteLine(scanImage);
                }
		}
		
	    [STAThread]
	    public void executeScan()
	    {
			this.scan();
	    	Application.Run(this);
			
	    }
		
		
		// Resolution in monitor
		[DllImport("gdi32.dll", ExactSpelling=true)]
		private static extern int GetDeviceCaps( IntPtr hDC, int nIndex );
	
		[DllImport("gdi32.dll", CharSet=CharSet.Auto)]
		private static extern IntPtr CreateDC( string szdriver, string szdevice, string szoutput, IntPtr devmode );
	
		[DllImport("gdi32.dll", ExactSpelling=true)]
		private static extern bool DeleteDC( IntPtr hdc );
		
		public static int ScreenBitDepth {
			get {
				IntPtr screenDC = CreateDC( "DISPLAY", null, null, IntPtr.Zero );
				int bitDepth = GetDeviceCaps( screenDC, 12 );
				bitDepth *= GetDeviceCaps( screenDC, 14 );
				DeleteDC( screenDC );
				return bitDepth;
			}
		}
		
		// Windows specific function to interact with system
		//private Win32.WINMSG	winmsg;
		//private IntPtr	hwnd;
		//private Identity	appid;
		//private Identity	srcds;
		//private Event		evtmsg;
		
		// Interpretazione dei messaggi per la gestione
		/*
		public TwainCommand PassMessage( ref Message m )
		{
			if( srcds.Id == IntPtr.Zero )
				return TwainCommand.Not;
	
			int pos = GetMessagePos();
	
			winmsg.hwnd		= m.HWnd;
			winmsg.message	= m.Msg;
			winmsg.wParam	= m.WParam;
			winmsg.lParam	= m.LParam;
			winmsg.time		= GetMessageTime();
			winmsg.x		= (short) pos;
			winmsg.y		= (short) (pos >> 16);
			
			Marshal.StructureToPtr( winmsg, evtmsg.EventPtr, false );
			evtmsg.Message = 0;
			TwRC rc = DSevent( appid, srcds, TwDG.Control, TwDAT.Event, TwMSG.ProcessEvent, ref evtmsg );
			if( rc == TwRC.NotDSEvent )
				return TwainCommand.Not;
			if( evtmsg.Message == (short) TwMSG.XFerReady )
				return TwainCommand.TransferReady;
			if( evtmsg.Message == (short) TwMSG.CloseDSReq )
				return TwainCommand.CloseRequest;
			if( evtmsg.Message == (short) TwMSG.CloseDSOK )
				return TwainCommand.CloseOk;
			if( evtmsg.Message == (short) TwMSG.DeviceEvent )
				return TwainCommand.DeviceEvent;
	
			return TwainCommand.Null;
		}
		*/
	
	}
	
	class Win32
	{
		
		[StructLayout(LayoutKind.Sequential, Pack=4)]
		internal struct WINMSG
		{
			public IntPtr		hwnd;
			public int			message;
			public IntPtr		wParam;
			public IntPtr		lParam;
			public int			time;
			public int			x;
			public int			y;
		}
		
	}
	
	
}

