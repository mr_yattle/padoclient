﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace TwainDotNet
{
    public class WindowException : ApplicationException
    {
        public WindowException():
			base(null, null)
        {
        }

        public WindowException(string message): 
			base(message, null)
        {
        }

    }
	
    public class WithoutDataSourceException : ApplicationException
    {
        public WithoutDataSourceException(string message): 
			base(message, null)
        {
        }
    }
	
    public class CloseWindowException : ApplicationException
    {
        public CloseWindowException(string message): 
			base(message, null)
        {
        }
    }
	
    public class ScanFailedWindowException : ApplicationException
    {
        public ScanFailedWindowException(string message): 
			base(message, null)
        {
        }
    }
	
	
}
