using System;
using System.Collections.Generic;
using NDesk.Options;
using System.IO;
using System.Drawing;

namespace win32scanner
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			// Scanner models
			// FUJITSU%fi-5750Cdj
			// WIA-fi-5750Cdj
			
			bool show_help = false;
			string options = "";
			string model = null;
			TwainScanner scanner = null;
			IScanSettings settings = new IScanSettings();
			
			var p = new OptionSet () {
		      { "l|list=", "increase debug message verbosity",
		        (string v) => options = v },
			  { "m|model=", "set TWAIN model ad datasource to scan for list of avaible devices user list command",
		        (string v) => { if (v != null) model = v; } },
			  { "f|format=", "set paper format",
		        (string v) => { if (v != null) settings.paperFormat(v); } },
			  { "w|orientation=", "set paper orientation",
		        (string v) => { if (v != null) settings.orientation(v); } },
			  { "d|dpi=", "set dpi resolution for images",
		        (string v) => { if (v != null) settings.dpi(v); } },
			  { "a|adf", "get paper from automatic document feeder",
		        v => settings.UseDocumentFeeder = v != null },
			  { "x|duplex", "scan page front and rear",
		        v => settings.UseDuplex = v != null },
			  { "g|grayscale", "scan page in grayscale",
		        v => { if (v != null) settings.grayscale(); } },
			  { "u|gui", "scan page using scanner UI",
		        v => settings.ShowTwainUI = v != null },
			  { "c|color", "scan page in coloured mode",
		        v => { if (v != null) settings.color(); } },
			  { "b|bw", "scan page in black & white mode",
		        v => { if (v != null) settings.blackandwhite(); } },
		      { "h|help",  "show this message and exit", 
		        v => show_help = v != null },
			};
			// settings.UseDuplex = true;
			
			List<string> extra;
		  	try {
		      extra = p.Parse (args);
		  	}
		  	catch (OptionException e) {
		      	DefaultMessage(e);
				return;
		  	}

	        if (show_help) {
	            ShowHelp (p);
	            return;
	        }
			
			string command = null;
	        if (extra.Count > 0) {
	            command = string.Join (" ", extra.ToArray ());
	            //Debug ("Using new message: {0}", command);
	        }
			
			// No command specified
			if(command == null) {
				DefaultMessage();
				return;
			}

			// Now we can use commands
			scanner = new TwainScanner(true);
			
			// Launch command 
			Console.WriteLine ();
			switch(command.ToLower()) {
			    case "scan":
					// Console cannot handle spaces very well
				    // Need to ptimize if different than current
					if(model != null) {
						string _model = model.Replace("%", " ");
						scanner.chooseBeforeScan(_model);
					}
					scanner.scan(settings);
					save(scanner);
			        break;
			    case "formats":
					formats(settings);
			        break;
				case "orientations": 
			        orientations(settings);
			        break;
				case "dpi": 
			        dpi(settings);
			        break;
			    case "list":
					list(scanner);
			        break;
				case "select": 
					choose(scanner);
			        break;
				case "current": 
			        current(scanner);
			        break;
				case "capabilities":
					string saveDir = System.AppDomain.CurrentDomain.BaseDirectory+"/Capabilites";
					string savePath = System.IO.Path.GetFullPath(saveDir+"/"+scanner.scannerName()+".xml");
			        string _capabilites = capabilities(scanner);
					if(_capabilites == null) {
						Console.WriteLine("Cannot use this TWAIN driver");
					}
					else {
						try {
							FileInfo xml = new FileInfo(savePath);
							StreamWriter stream = xml.CreateText();
							stream.WriteLine(_capabilites);
							stream.Close();
						}	
						catch(System.IO.IOException ioex) {
							// do nothing probably log file is unaccessible cause user is reading it
							Console.WriteLine(ioex.ToString());
						}
					}
			        break;
			    default:
			        DefaultMessage();
			        break;
			}
			
		}
			
	    static void ShowHelp (OptionSet p)
	    {
	        Console.WriteLine ("Usage: win32.scanner [OPTIONS]+ command");
	        Console.WriteLine ("Test scanner functionality.");
	        Console.WriteLine ("If no command is specified, list all avaible commands.");
	        Console.WriteLine ();
			Console.WriteLine ("Commands:");
			Console.WriteLine ("  scan                       acquire image from scanner");
			Console.WriteLine ("  formats                    show paper formats possibile values");
			Console.WriteLine ("  orientations               show paper formats possible orientation");
			Console.WriteLine ("  dpi                        show possible dpi values");
			Console.WriteLine ("  list                       list alla avaible TWAIN scanner");
			Console.WriteLine ("  select                     set TWAIN scanner model to scan otherwise, OS auto select it");
			Console.WriteLine ("  current                    get from OS current TWAIN datasource ");
			Console.WriteLine ("  capabilities               execute dignostic on scanner to test capabilities");
			Console.WriteLine ();
	        Console.WriteLine ("Scan options:");
	        p.WriteOptionDescriptions (Console.Out);
	    }
		
	    static void DefaultMessage ()
	    {
	      Console.Write ("win32.scanner: ");
	      Console.WriteLine ("Try `win32.scanner --help' for more information.");
	    }
		
	    static void DefaultMessage (OptionException e)
	    {
	      Console.Write ("win32.scanner: ");
	      Console.WriteLine (e.Message);
	      Console.WriteLine ("Try `win32.scanner --help' for more information.");
	    }
		
	    static void list (TwainScanner scanner)
	    {
			IList<string> avaibles = scanner.list();
			if(avaibles == null)
				Console.WriteLine ("Turn on devices or missing device TWAIN drivers");
			else {
				Console.WriteLine ("Avaible TWAIN devices:");
				foreach(string model in avaibles){
					Console.WriteLine (" {0}", model);
				}
			}	
	    }
		
	    static void choose (TwainScanner scanner)
	    {
			//string _model = model.Replace("%", " ");
			if(scanner.choose()) {
				current(scanner);
			}
			else {
				Console.WriteLine ("No installed scanner detected !");
			}
	    }
		
	    static void current (TwainScanner scanner)
	    {
			Console.WriteLine ("Current selected TWAIN datasource: {0}", scanner.scannerName());
	    }
		
	    static void save (TwainScanner scanner)
	    {			
			string timestamp = DateTime.Now.ToString();
			timestamp = timestamp.Replace("/", "-");
			string saveDir = System.AppDomain.CurrentDomain.BaseDirectory+"/Data";
			foreach(string f in System.IO.Directory.GetFiles(System.IO.Path.GetFullPath(saveDir)))
			{
				System.IO.File.Delete(f);
			}
			
			int counter = 0;
			foreach(Image image in scanner.Images){
				counter++;
				Console.WriteLine ("Image {0}: ", image.ToString());
				Console.WriteLine (" Type Bitmap (change to gif, tiff ....)");
				Console.WriteLine (" Height {0}",image.Height);
				Console.WriteLine (" Width {0}",image.Width);
				string savepath = System.IO.Path.GetFullPath(saveDir+"/"+timestamp+"-"+counter.ToString()+".tiff");
				image.Save(savepath, System.Drawing.Imaging.ImageFormat.Tiff);
				Console.WriteLine (" Save To: {0}", savepath);
			}
	    }
		
	    static string capabilities (TwainScanner scanner)
	    {
			MyDiagnostics diagnostic =  scanner.diagnostic();
			ScannerInfo info = diagnostic.info();
			List<CapabilityItem> capabilites = diagnostic.capabilities();
			if(capabilites.Count == 0)
				return null;
			string xml = "<?xml version=\"1.0\"?>" + "\r\n";
			xml += "<device>" + "\r\n";
			xml += " <scanner>" + "\r\n";
			xml += "  <source manufactor=\""+info.Manufactor+"\" family=\""+info.ProductFamily+"\" name=\""+info.ProductName+"\" />" + "\r\n";
			xml += "  <capabilities>" + "\r\n";
			foreach (CapabilityItem capability in capabilites) {
				xml += "   <capability name=\""+capability.name.ToString()+"\" supported=\""+capability.supported.ToString()+"\" />" + "\r\n";
				//Console.WriteLine ("Support '{0}': {1}", 
				//capability.name.ToString(), capability.supported.ToString());
			}
			xml += "  </capabilities>" + "\r\n";
			xml += " </scanner>" + "\r\n";
			xml += "</device>" + "\r\n";
			return xml;
	    }
		
	    static void orientations (IScanSettings settings)
	    {
			Console.WriteLine ("Supported oritntations:");
			foreach(string ori in settings.orientations().Keys) {
				Console.WriteLine ("{0} ", ori);
			}
	    }
		
	    static void formats (IScanSettings settings)
	    {
			Console.WriteLine ("Supported formats:");
			foreach(string format in settings.formats().Keys) {
				Console.WriteLine ("{0} ", format);
			}
	    }
		
	    static void dpi (IScanSettings settings)
	    {
			Console.WriteLine ("Supported dpi:");
			foreach(string dpi in settings.dpis().Keys) {
				Console.WriteLine ("{0} ", dpi);
			}
	    }
		
	}
}

